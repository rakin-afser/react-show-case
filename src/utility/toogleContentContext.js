import React, { useState, createContext } from "react";
import templateConfig from "./templateConfig";



const FoldedContentContext = createContext();

function FoldedContentProvider(props) {
  const [foldedContent, setFoldedContent] = useState(templateConfig.sidebar.collapsed)
  const [makeFullContent, setMakeFullContent] = useState(() => {
    setFoldedContent(true);
 });
 const [makeNormalContent, setMakeNormalContent] = useState(()=> {
   setFoldedContent(false)
 });

  //  state = {
  //     foldedContent: templateConfig.sidebar.collapsed,
  //     makeFullContent: () => {
  //        this.setState(prevState => ({
  //           foldedContent: true
  //        }));
  //     },
  //     makeNormalContent: () => {
  //        this.setState(prevState => ({
  //           foldedContent: false
  //        }));
  //     }
  //  };

   // render() {
      return (
         <FoldedContentContext.Provider value={{foldedContent, makeFullContent, makeNormalContent }}>
            {props.children}
         </FoldedContentContext.Provider>
      );
   // }
}
const FoldedContentConsumer = FoldedContentContext.Consumer;

export { FoldedContentProvider, FoldedContentConsumer}