import React, { Suspense } from "react";
import "./common.css";
import { Provider } from "react-redux";
import { ToastContainer } from "react-toastify";
import store from "./redux/store";
import SpinnerComponent from "./components/common/Spinner.js";
import Route from './Route'
import "react-perfect-scrollbar/dist/css/styles.css";
function App() {
  return (
    <Provider store={store}>
    <Suspense fallback={SpinnerComponent}>
          <Route />
        </Suspense>
    </Provider>
  );
}

export default App;

