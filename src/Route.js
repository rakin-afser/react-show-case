import React, { Suspense, lazy } from "react";
import { BrowserRouter, Switch } from "react-router-dom";
import SpinnerComponent from "./components/common/Spinner";
import MainLayoutRoute from './components/layout/mainRoutes';
import FullPageLayout from "./components/layout/fullpageRoutes";
const LazyLogin = lazy(() => import("./components/pages/Login"));
const LazyEmployee = lazy(() => import("./components/pages/Employee"));
const LazyCompanyinfo = lazy(() => import("./components/pages/Companyinfo"));
const LazyUserprofile = lazy(() => import("./components/pages/Userprofile"));
const LazySmsVerification = lazy(() => import("./components/pages/smsVerification"));
const LazyUsers = lazy(() => import("./components/pages/Users"));
const LazyPermissions = lazy(() => import("./components/pages/Permissions"));
const LazyCreateIndividualPayment = lazy(() => import("./components/Transactions/CreatePayments/CreateIndividualDisbursement"));
const LazyCreateBulkDisbursement = lazy(() => import("./components/Transactions/CreatePayments/createBulkDisbursement"));
const LazyCreateWPSBulkDisbursement = lazy(() => import("./components/Transactions/CreatePayments/createWPSBulkDisbursement"));
const LazyUploadTransferProof = lazy(() => import("./components/Transactions/uploadTransferProof"))
const LazyViewPayments = lazy(() => import("./components/Transactions/ViewPayments"));
const LazyAuthBulkPayment = lazy(() => import("./components/Transactions/AuthorizePayments/AuthBulkPayment"));
const LazyAuthIndividualPayment = lazy(() => import("./components/Transactions/AuthorizePayments/AuthIndividualPayment"));
const LazyAuthWpsPayments = lazy(() => import("./components/Transactions/AuthorizePayments/AuthWpsPayments"));
const LazyValidBulkPayment = lazy(() => import("./components/Transactions/ValidatePayments/ValidBulkPayment"));
const LazyValidIndividualPayment = lazy(() => import("./components/Transactions/ValidatePayments/ValidIndividualPayment"));
const LazyValidWpsPayment = lazy(() => import("./components/Transactions/ValidatePayments/ValidWpsPayment"));
const LazyWpsStatus = lazy(() => import("./components/Transactions/WpsStatus"));
const LazyStatusReport = lazy(() => import("./components/Transactions/StatusReport"));
export default function Route () {
  return (
    // Set the directory path if you are deplying in sub-folder
    <BrowserRouter basename="/">
       <Switch>
          {/* Dashboard Views */}
          
          {/** Full Page route /login */}
          <FullPageLayout
          exact
          path="/login"
          render={matchprops => (
             <Suspense fallback={<SpinnerComponent />}>
                <LazyLogin {...matchprops} />
             </Suspense>
          )}
         />
         <FullPageLayout
         exact
         path="/smsVerification"
         render={matchprops => (
            <Suspense fallback={<SpinnerComponent />}>
               <LazySmsVerification {...matchprops} />
            </Suspense>
         )}
         />
         <FullPageLayout
         exact
         path="/forgot-password"
         render={matchprops => (
            <Suspense fallback={<SpinnerComponent />}>
               <LazyLogin {...matchprops} />
            </Suspense>
         )}
         />
         <FullPageLayout
            exact
            path="/register"
            render={matchprops => (
               <Suspense fallback={<SpinnerComponent />}>
                  <LazyLogin {...matchprops} />
               </Suspense>
               )}
            />
          <MainLayoutRoute
             exact
             path="/employees"
             render={matchprops => (
                <Suspense fallback={<SpinnerComponent />}> 
                     <p></p>              
                   <LazyEmployee {...matchprops} />                 
                </Suspense>
             )}
          />
      
         <MainLayoutRoute
             exact
             path="/reviweCompanyInfo"
             render={matchprops => (
                <Suspense fallback={<SpinnerComponent />}>
                   <LazyCompanyinfo {...matchprops} />
                </Suspense>
             )}
          /> 
          <MainLayoutRoute
             exact
             path="/user-profile"
             render={matchprops => (
                <Suspense fallback={<SpinnerComponent />}>
                   <LazyUserprofile {...matchprops} />
                </Suspense>
             )}
          /> 
          <MainLayoutRoute
             exact
             path="/users"
             render={matchprops => (
                <Suspense fallback={<SpinnerComponent />}>
                   <LazyUsers {...matchprops} />
                </Suspense>
             )}
          />
         
          <MainLayoutRoute
             exact
             path="/permissions"
             render={matchprops => (
                <Suspense fallback={<SpinnerComponent />}>
                   < LazyPermissions {...matchprops} />
                </Suspense>
             )}
          />
          <MainLayoutRoute
             exact
             path="/createIndividualDisbursement"
             render={matchprops => (
                <Suspense fallback={<SpinnerComponent />}>
                   < LazyCreateIndividualPayment {...matchprops} />
                </Suspense>
             )}
          />
          
          <MainLayoutRoute
             exact
             path="/createBulkDisbursement"
             render={matchprops => (
                <Suspense fallback={<SpinnerComponent />}>
                   < LazyCreateBulkDisbursement {...matchprops} />
                </Suspense>
             )}
          />
            <MainLayoutRoute
             exact
             path="/createWPSBulkDisbursement"
             render={matchprops => (
                <Suspense fallback={<SpinnerComponent />}>
                   < LazyCreateWPSBulkDisbursement {...matchprops} />
                </Suspense>
             )}
          />
           <MainLayoutRoute
             exact
             path="/uploadTransferProof"
             render={matchprops => (
                <Suspense fallback={<SpinnerComponent />}>
                   < LazyUploadTransferProof {...matchprops} />
                </Suspense>
             )}
          />
           <MainLayoutRoute
             exact
             path="/viewPayment"
             render={matchprops => (
                <Suspense fallback={<SpinnerComponent />}>
                   < LazyViewPayments {...matchprops} />
                </Suspense>
             )}
          />
          <MainLayoutRoute
             exact
             path="/authorizeBulkPayment"
             render={matchprops => (
                <Suspense fallback={<SpinnerComponent />}>
                   < LazyAuthBulkPayment {...matchprops} />
                </Suspense>
             )}
          />
          <MainLayoutRoute
             exact
             path="/authorizeIndividualPayment"
             render={matchprops => (
                <Suspense fallback={<SpinnerComponent />}>
                   <LazyAuthIndividualPayment {...matchprops} />
                </Suspense>
             )}
          />
          <MainLayoutRoute
             exact
             path="/authorizeWpsPayment"
             render={matchprops => (
                <Suspense fallback={<SpinnerComponent />}>
                   <LazyAuthWpsPayments {...matchprops} />
                </Suspense>
             )}
          />
           <MainLayoutRoute
             exact
             path="/validateBulkPayment"
             render={matchprops => (
                <Suspense fallback={<SpinnerComponent />}>
                   <LazyValidBulkPayment  {...matchprops} />
                </Suspense>
             )}
          />
           <MainLayoutRoute
             exact
             path="/validateIndividualPayment"
             render={matchprops => (
                <Suspense fallback={<SpinnerComponent />}>
                   <LazyValidIndividualPayment {...matchprops} />
                </Suspense>
             )}
          />
           <MainLayoutRoute
             exact
             path="/validateWpsPayment"
             render={matchprops => (
                <Suspense fallback={<SpinnerComponent />}>
                   <LazyValidWpsPayment {...matchprops} />
                </Suspense>
             )}
          />
          <MainLayoutRoute
             exact
             path="/wpsStatus"
             render={matchprops => (
                <Suspense fallback={<SpinnerComponent />}>
                   <LazyWpsStatus {...matchprops} />
                </Suspense>
             )}
          />
           <MainLayoutRoute
             exact
             path="/statusReport"
             render={matchprops => (
                <Suspense fallback={<SpinnerComponent />}>
                   <LazyStatusReport {...matchprops} />
                </Suspense>
             )}
          />


           <MainLayoutRoute
             exact
             path="/"
             render={matchprops => (
                <Suspense fallback={<SpinnerComponent />}>
                   {/**
                   Add Dashboard Component 
                  */}
                        
                </Suspense>
             )}
          />
      
          {/* Single Views */}
          
       </Switch>
    </BrowserRouter>
 );
}