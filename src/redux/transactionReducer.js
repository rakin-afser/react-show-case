import types from "../actions/types";

const initialState = {
  isLoggedIn: false,
  isLoading: true,
  createpaymentBulk: [],
  validateIndividualPayment: [],
  validateBulkPayment: [],
  validateWpsPayment: [],
  authBulkPayment:[],
  authIndividualPayment:[],
  authWpsPayment:[],
  balance:"",
  downloadFile:[]

};

export default function (state = initialState, action) {
  const { type, payload } = action;

  switch (type) {

    case types.CREATE_INDIVIDUAL_PAYMENT_SUCCESS:
      return {
        ...state,
        createpaymentBulk: payload,
        isLoading: false,
      };
      break;
    case types.CREATE_INDIVIDUAL_PAYMENT_FAILED:
      return {
        ...state,
        isLoading: false,
      };
      break;
    case types.CREATE_BULK_PAYMENT:
      return {
        ...state,
        isLoading: true,
      };

      break;
    case types.CREATE_BULK_PAYMENT_SUCCESS:
      return {
        ...state,
        payment: payload,
        isLoading: false,
      };
      break;
    case types.CREATE_BULK_PAYMENT_FAILED:
      return {
        ...state,
        isLoading: false,
      };
      break;
    case types.CREATE_INDIVIDUAL_VALIDATE_PAYMENT:
      return {
        ...state,
        isLoading: true,
      };
      break;
    case types.CREATE_INDIVIDUAL_VALIDATE_PAYMENT_SUCCESS:
      return {
        ...state,
        createpaymentBulk: payload,
        isLoading: false,
      };
    case types.CREATE_INDIVIDUAL_VALIDATE_PAYMENT_FAILED:
      return {
        ...state,
        isLoading: false,
      };
      break;
    case types.FETCH_VALIDATE_INDIVIDUAL:
      return {
        ...state,
        isLoading: true,
      };
      break;
    case types.FETCH_VALIDATE_INDIVIDUAL_SUCCESS:
      return {
        ...state,
        validateIndividualPayment: payload,
        isLoading: false,
      };
    case types.FETCH_VALIDATE_INDIVIDUAL_FAILED:
      return {
        ...state,
        isLoading: false,
      };
      break;
    case types.FETCH_VALIDATE_BULK:
      return {
        ...state,
        isLoading: true,
      }
      break;
    case types.FETCH_VALIDATE_BULK_SUCCESS:
      return {
        ...state,
        validateBulkPayment: payload,
        isLoading: false,
      };
    case types.FETCH_VALIDATE_BULK_FAILED:
      return {
        ...state,
        isLoading: false,
      }
      break;
    case types.FETCH_VALIDATE_WPS:
      return {
        ...state,
        isLoading: true,
      }
      break;
    case types.FETCH_VALIDATE_WPS_SUCCESS:
      return {
        ...state,
        validateWpsPayment: payload,
        isLoading: false,
      }
      break;
    case types.FETCH_VALIDATE_WPS_FAILED:
      return {
        ...state,
        isLoading: false,
      }
      break;
      case types.FETCH_AUTHORIZE_BULK_PAYMENT:
      return {
        ...state,
        isLoading: true,
      }
      break;
    case types.FETCH_AUTHORIZE_BULK_PAYMENT_SUCCESS:
      return {
        ...state,
        authBulkPayment: payload,
        isLoading: false,
      }
      break;
    case types.FETCH_AUTHORIZE_BULK_PAYMENT_FAILED:
      return {
        ...state,
        isLoading: false,
      }
      break;
      case types.FETCH_AUTHORIZE_INDIVIDUAL_PAYMENT:
      return {
        ...state,
        isLoading: true,
      }
      break;
    case types.FETCH_AUTHORIZE_INDIVIDUAL_PAYMENT_SUCCESS:
      return {
        ...state,
        authIndividualPayment: payload,
        isLoading: false,
      }
      break;
    case types.FETCH_AUTHORIZE_INDIVIDUAL_PAYMENT_FAILED:
      return {
        ...state,
        isLoading: false,
      }
      break;
      case types.FETCH_AUTHORIZE_WPS_PAYMENT:
      return {
        ...state,
        isLoading: true,
      }
      break;
    case types.FETCH_AUTHORIZE_WPS_PAYMENT_SUCCESS:
      return {
        ...state,
        authWpsPayment: payload,
        isLoading: false,
      }
      break;
    case types.FETCH_AUTHORIZE_WPS_PAYMENT_FAILED:
      return {
        ...state,
        isLoading: false,
      }
      break;
      case types.VALIDATE_VALID_BULK_PAYMENT_SUCCESS:
        return {
          ...state,
          validateBulkPayment: payload,
          isLoading: false,
        }
        break;
      case types.VALIDATE_VALID_BULK_PAYMENT_FAILED:
        return {
          ...state,
          isLoading: false,
        }
        break;
        case types.REJECT_VALIDATE_BULK_PAYMENT_SUCCESS:
        return {
          ...state,
          validateBulkPayment: payload,
          isLoading: false,
        }
        break;
      case types.REJECT_VALIDATE_BULK_PAYMENT_FAILED:
        return {
          ...state,
          isLoading: false,
        }
        break;

        case types.VALIDATE_VALID_INDIVIDUAL_PAYMENT_SUCCESS:
          return {
            ...state,
            validateIndividualPayment: payload,
            isLoading: false,
          }
          break;
        case types.VALIDATE_VALID_INDIVIDUAL_PAYMENT_FAILED:
          return {
            ...state,
            isLoading: false,
          }
          break;
          case types.REJECT_VALIDATE_INDIVIDUAL_PAYMENT_SUCCESS:
          return {
            ...state,
            validateIndividualPayment: payload,
            isLoading: false,
          }
          break;
        case types.REJECT_VALIDATE_INDIVIDUAL_PAYMENT_FAILED:
          return {
            ...state,
            isLoading: false,
          }
          break;
          case types.REJECT_AUTHORIZE_BULK_PAYMENT_SUCCESS:
          return {
            ...state,
            authBulkPayment: payload,
            isLoading: false,
          }
          break;
        case types.REJECT_AUTHORIZE_BULK_PAYMENT_FAILED:
          return {
            ...state,
            isLoading: false,
          }
          break;
          case types.REJECT_AUTHORIZE_INDIVIDUAL_PAYMENT_SUCCESS:
          return {
            ...state,
            authIndividualPayment: payload,
            isLoading: false,
          }
          break;
        case types.REJECT_AUTHORIZE_INDIVIDUAL_PAYMENT_FAILED:
          return {
            ...state,
            isLoading: false,
          }
          break;

          case types.FETCH_BALANCE:
      return {
        ...state,
        isLoading: true,
      }
      break;
    case types.FETCH_BALANCE_SUCCESS:
      return {
        ...state,
        balance: payload,
        isLoading: false,
      }
      break;
    case types.FETCH_BALANCE_FAILED:
      return {
        ...state,
        isLoading: false,
      }
      break;
      case types.APPROVE_AUTHORIZE_BULK_PAYMENT_SUCCESS:
        return {
          ...state,
          authBulkPayment: payload,
          isLoading: false,
        }
        break;
      case types.APPROVE_AUTHORIZE_BULK_PAYMENT_FAILED:
        return {
          ...state,
          isLoading: false,
        }
        break;
        case types.APPROVE_AUTHORIZE_INDIVIDUAL_PAYMENT_SUCCESS:
          return {
            ...state,
            authIndividualPayment: payload,
            isLoading: false,
          }
          break;
        case types.APPROVE_AUTHORIZE_INDIVIDUAL_PAYMENT_FAILED:
          return {
            ...state,
            isLoading: false,
          }
          break;
          case types.FETCH_DOWNLOAD_FILE:
      return {
        ...state,
        isLoading: true,
      }
      break;
    case types.FETCH_DOWNLOAD_FILE_SUCCESS:
      return {
        ...state,
        downloadFile: payload,
        isLoading: false,
      }
      break;
    case types.FETCH_DOWNLOAD_FILE_FAILED:
      return {
        ...state,
        isLoading: false,
      }
      break;
    default:
      return state;
  }
}