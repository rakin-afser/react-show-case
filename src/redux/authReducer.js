import types from "../actions/types";

const initialState = {
  user: null,
  isLoggedIn: false,
  isLoading: true,
};

export default function (state = initialState, action) {
  const { type, payload } = action;

  switch (type) {
    case types.FETCH_USER:
      return {
        ...state,
        isLoading: true,
      };

      break;
    case types.FETCH_USER_SUCCESS:
      return {
        ...state,
        user: payload,
        isLoading: false,
      };
      break;
    case types.FETCH_USER_FAILED:
      return {
        ...state,
        isLoading: false,
      };
      break;
    case types.SMS_VERIFICATION_FAILD:
      return {
        ...state,
        user: null,
        isLoggedIn: false,
        isLoading: false,
      };

    default:
      return state;
  }
}
