import { combineReducers } from "redux";
import appReducer from "./appReducer";
import authReducer from "./authReducer";
import customizer from './customizeReducer/customizeReducer';
import transactionReducer from "./transactionReducer";
export default combineReducers({
  appReducer,
  authReducer,
  customizer,
  transactionReducer
});
