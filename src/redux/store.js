import { createStore, applyMiddleware } from "redux";
import { composeWithDevTools } from "redux-devtools-extension";
import thunk from "redux-thunk";
import { routerMiddleware } from 'react-router-redux'
import rootReducder from "./rootReducer";
import { createBrowserHistory } from "history"
const browserhistory = createBrowserHistory();
const initialState = {};
const middleware = [thunk, routerMiddleware(browserhistory)];
if (process.env.NODE_ENV === `development`) {
  const { logger } = require(`redux-logger`);

  middleware.push(logger);
}
const store = createStore(
  rootReducder,
  initialState,
  composeWithDevTools(applyMiddleware(...middleware))
);
export default store;
