import http from "../services/http";
import types from "./types";
import messages from "../services/messages";
import { checkSuccessStatus } from "./authHelper";

export const login = (username, password, history) => async (dispatch) => {
  try {
    const res = await http.post(
      `/login`,
      { username, password },
      {
        withCredentials: true,
      }
    );

    if (checkSuccessStatus(res.status)) {
      if (res.data.status === "2FA") {
        const id = res.data.id;
        history.push(`/smsVerification?id=${id}`);
      } else {
        history.push("/");
      }
    }
  } catch (error) {
    console.log("Error catch block here", error);
    dispatch({ type: types.LOGIN_FAILED });

  }
};

export const smsVerification = (id, smsCode, history) => async (dispatch) => {
  try {
    console.log(id, smsCode, history);
    const res = await http.post(
      `/smsVerification`,
      { smsCode, id },
      {
        withCredentials: true,
      }
    );

    if (checkSuccessStatus(res.status)) {
      history.push("/");
    }
  } catch (error) {
    messages.error(error);
    
    dispatch({ type: types.SMS_VERIFICATION_FAILD });


  }
};

export const getUser = () => async (dispatch) => {
  try {
    dispatch({ type: types.FETCH_USER })
    const userApiResponse = await http.get(`/api/v1/user`);
    if (checkSuccessStatus(userApiResponse.status)) {
      dispatch({
        type: types.FETCH_USER_SUCCESS,
        payload: userApiResponse.data
      });

    }

  } catch (error) {
    messages.error(error);
    console.log("Error catch block here", error);
    dispatch({ type: types.FETCH_USER_FAILED })
  }
};


export const addUser = (fullName, username, mobileNumber, email, idCardNumber, roleId) => async (dispatch) => {
  try {
    const userPostApiResponse = await http.post(
      `/api/v1/user`,
      { fullName, username, mobileNumber, email, idCardNumber, roleId },
      {
        withCredentials: true,
      }
    ); 
    if (checkSuccessStatus(userPostApiResponse.status)) {
      dispatch({
        type: types.FETCH_USER_SUCCESS,
        payload: userPostApiResponse.data
      });

    } else {
      dispatch({ type: types.FETCH_USER_FAILED })
    }
  } catch (error) {
    messages.error(error);
    console.log("Error catch block here", error);
  }
};

export const getUserList = () => async (dispatch) =>{
  try{
      dispatch({type:types.FETCH_USER_LIST})
      const userlistApiRes =await http.get(
          `/api/v1/user/list`,
          {
              withCredentials:true,
          }
      );
      if(checkSuccessStatus(userlistApiRes.status)){
          dispatch({
              type:types.FETCH_USER_LIST_SUCCESS,
              payload: userlistApiRes.data
          })
      }else{
          dispatch({type: types.FETCH_USER_LIST_FAILED})
      }

  }catch(error){
      dispatch({type: types.FETCH_USER_LIST_FAILED});
  }
};
export const getRole = () => async (dispatch) => {
  try {
    dispatch({ type: types.FETCH_ROLES })
    const roleApiResponse = await http.get(
      `/api/v1/role`,
      {
        withCredentials: true,
      }
    );
    if (checkSuccessStatus(roleApiResponse.status)) {
      dispatch({
        type: types.FETCH_ROLES_SUCCESS,
        payload: roleApiResponse.data
      });

    } else {
      dispatch({ type: types.FETCH_ROLES_FAILED })
    }
  } catch (error) {
    messages.error(error);
    console.log("Error catch block here", error);
  }
};

export const updateRole = ( userId, roleId) => async (dispatch) => {
  let params={}
  // if(active = (undefined || 'undefined')) {
  //   params['active'] = active
  // }
  params['role'] = roleId

  try {
    const userUpdateApiRes = await http.post(
      `/api/v1/user/${userId}/update`,
      params,
      {
        withCredentials: true,
      }
    );
   
    if (checkSuccessStatus(userUpdateApiRes.status)) {
      dispatch({
        type: types.FETCH_ROLES_UPDATE_SUCCESS,
        payload: userUpdateApiRes.data
      });
      dispatch(getUserList())

    } else {
      dispatch({ type: types.FETCH_ROLES_UPDATE_FAILED })
    }
  } catch (error) {
    messages.error(error);
    console.log("Error catch block here", error);
  }
};

export const updateRoleStatus = ( userId, active) => async (dispatch) => {

  try {
    const resRoleStatus = await http.post(
      `/api/v1/user/${userId}/update`,
      active,
      {
        withCredentials: true,
      }
    );
    if (checkSuccessStatus(resRoleStatus.status)) {
      dispatch({
        type: types.UPDATE_ROLES_STATUS_SUCCESS,
        payload: resRoleStatus.data
      });
      dispatch(getUserList())
    } else {
      dispatch({ type: types.UPDATE_ROLES_STATUS_FAILED })
    }
  } catch (error) {
    messages.error(error);
    console.log("Error catch block here", error);
  }
};

export const updatePermission = () => async (dispatch) => {

  try {
    const resPermission = await http.post(
      `/api/v1/role/{id}/update`,
     
      {
        withCredentials: true,
      }
    );
    
    if (checkSuccessStatus(resPermission.status)) {
      dispatch({
        type: types.UPDATE_PERMISSION_SUCCESS,
        payload: resPermission.data
      });

    } else {
      dispatch({ type: types.UPDATE_PERMISSION_FAILED})
    }
  } catch (error) {
    messages.error(error);
    console.log("Error catch block here", error);
  }
};
