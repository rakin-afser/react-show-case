
import http from "../services/http";
import types from "./types";
import { checkSuccessStatus } from "./authHelper";


export const getEmployeesData = () => async (dispatch) => {
  try {
    dispatch({ type: types.FETCH_EMPLOYEES })
    const res = await http.get(
      `/api/v1/user/employees`,
      {
        withCredentials: true,
      }
    );

    if (checkSuccessStatus(res.status)) {
      dispatch({
        type: types.FETCH_EMPLOYEES_SUCCESS,
        payload: res.data
      })
    } else {
      dispatch({
        type: types.FETCH_EMPLOYEES_FAILD,
        
      })
    }

  } catch (error) {
    console.log("Error catch block here", error);
    dispatch({ type: types.FETCH_EMPLOYEES_FAILD });
  }
};

export const getSearch = (searchText) => async (dispatch) => {
  const searchApiResponse = await http.get(`/api/v1/user/employees?searchText=${searchText}`, {
    withCredentials: true
  })
  console.log('search result  ', searchApiResponse)
  if (checkSuccessStatus(searchApiResponse.status)) {
    dispatch({
      type: types.FETCH_EMPLOYEES_SUCCESS,
      payload: searchApiResponse.data
    })

  } else {
    dispatch({
      type: types.FETCH_EMPLOYEES_FAILD,
    
    })
  }
}
export const uploadEmployee = (file, type, fileName) => async (dispatch) => {
  try {
    const formData = new FormData()
    formData.append('file', file);
    formData.append('bulkUpdate', false);
    formData.append('fileName', fileName)
    const uploadApiResponse = await http.post(
      'api/v1/file/EMPLOYEE',
      formData
      ,
      {
        withCredentials: true,
        headers: {
          'Content-type': 'application/x-www-form-urlencoded'
        }
        
      }
    );
      
    if (checkSuccessStatus(uploadApiResponse.status)) {
      // dispatch for success
    } else {
      
      // dispatch for failure
    }

  } catch (error) {
    console.log("Error catch block here", error);
    // dispatch for failure
  }
};



export const addEmployee = (name, id, mobile,department,position,refId) => async (dispatch) => {
  try {
    const resAddEmployee = await http.post(
      `/api/v1/file/EMPLOYEE`,
      { name, id, mobile,department,position,refId},
      {
        withCredentials: true,
      }
    );

    if (checkSuccessStatus(resAddEmployee.status)) {
      dispatch({
        type: types.FETCH_EMPLOYEES_SUCCESS,
        payload: resAddEmployee.data
      })
    }
  } catch (error) {
    console.log("Error catch block here", error);
    dispatch({ type: types.FETCH_EMPLOYEES_FAILD });


  }
};