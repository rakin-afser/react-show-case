import http from "../services/http";
import types from "./types";
import { checkSuccessStatus } from "./authHelper";


export const getCompanyProfile = () => async (dispatch) => {
  try {
    dispatch({ type: types.FETCH_COMPANY_PROFILE })

    const res = await http.get(
      `/reviewCompanyDetails`,
      {
        withCredentials: true,
      }
    );

    if (checkSuccessStatus(res.status)) {
      dispatch({
        type: types.FETCH_COMPANY_PROFILE_SUCCESS,
        payload: res.data
      })
    } else {
      dispatch({ type: types.FETCH_COMPANY_PROFILE_FAILED })


    }

  } catch (error) {
    console.log("Error catch block here", error);
    dispatch({ type: types.FETCH_COMPANY_PROFILE_FAILED });


  }
};




