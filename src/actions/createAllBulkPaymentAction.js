import http from "../services/http";
import types from "./types";
import messages from "../services/messages";
import { checkSuccessStatus } from "./authHelper";

export const createIndividualPayment = (name, id, salary, paymentType, description, scheduledFor) => async (dispatch) => {
  const body = {
    "jsonArr": [{
      "name": name,
      "id": id,
      "salary": salary,
      "paymentType": paymentType,
      "description": description
    }],
    "fileName": name, 
    "scheduledFor": scheduledFor
  }
 
  try {
    const resIndividualPayment = await http.post(
      `/api/v1/file/SALARY/create`,
      body,
      {
        withCredentials: true,
      }
    );

    if (checkSuccessStatus(resIndividualPayment.status)) {
      dispatch({
        type: types.CREATE_INDIVIDUAL_PAYMENT_SUCCESS,
        payload: resIndividualPayment.data
      });
    } else {
      dispatch({ type: types.CREATE_INDIVIDUAL_PAYMENT_FAILED })
    }
  } catch (error) {
    messages.error(error);
    console.log("Error catch block here", error);
    dispatch({ type: types.CREATE_INDIVIDUAL_PAYMENT_FAILED })

  }
};

export const createBulkPayment = (fmData,config) => async (dispatch) => {
  try {
    dispatch({ type: types.CREATE_BULK_PAYMENT })
    const resBulkPayment = await http.post(
      `/api/v1/file/SALARY`,
      fmData,
      {
        withCredentials: true,
        // headers: { "content-type": "multipart/form-data" }
        config
      }
    );

    if (checkSuccessStatus(resBulkPayment.status)) {
      dispatch({
        type: types.CREATE_BULK_PAYMENT_SUCCESS,
        payload: resBulkPayment.data
      });
    }
  } catch (error) {
    messages.error(error)
    console.log("Error catch block here", error);
    dispatch({ type: types.CREATE_BULK_PAYMENT_FAILED })

  }
};

export const createValidateIndividualPayment = (fileId) => async (dispatch) => {

  try {
    dispatch({ type: types.CREATE_INDIVIDUAL_VALIDATE_PAYMENT })
    const restValidaeIndividual = await http.post(
      `/api/v1/file/${fileId}/validateIndividual`,
      {
        withCredentials: true,
      }
    );

    if (checkSuccessStatus(restValidaeIndividual.status)) {
      dispatch({
        type: types.CREATE_INDIVIDUAL_VALIDATE_PAYMENT_SUCCESS,
        payload: restValidaeIndividual.data
      });
    }
  } catch (error) {
    messages.error(error)
    console.log("Error catch block here", error);
    dispatch({ type: types.CREATE_INDIVIDUAL_VALIDATE_PAYMENT_FAILED })

  }
};

export const getValidateIndividualPayment = () => async (dispatch) => {
  try {
    dispatch({ type: types.FETCH_VALIDATE_INDIVIDUAL })
    const restValidaeIndividual = await http.get(
      `/api/v1/file/validate/individual`,
      {
        withCredentials: true,
      }
    );

    if (checkSuccessStatus(restValidaeIndividual.status)) {
      dispatch({
        type: types.FETCH_VALIDATE_INDIVIDUAL_SUCCESS,
        payload: restValidaeIndividual.data
      })
    } else {
      dispatch({
        type: types.FETCH_VALIDATE_INDIVIDUAL_FAILED,

      })
    }

  } catch (error) {
    console.log("Error catch block here", error);
    dispatch({ type: types.FETCH_VALIDATE_INDIVIDUAL_FAILED });
  }
};

export const getValidateBulkPayment = () => async (dispatch) => {
  try {
    dispatch({ type: types.FETCH_VALIDATE_BULK })
    const resValidateBulk = await http.get(
      `api/v1/file/validate/bulk`,
      {
        withCredentials: true,
      }
    );
    if (checkSuccessStatus(resValidateBulk.status)) {
      dispatch({
        type: types.FETCH_VALIDATE_BULK_SUCCESS,
        payload: resValidateBulk.data
      })
    } else {
      dispatch({ type: types.FETCH_VALIDATE_BULK_FAILED })
    }
  } catch (error) {
    console.log("Error catch block here", error)
    dispatch({ type: types.FETCH_VALIDATE_BULK_FAILED })
  }
}

export const getValidateWpsPayments = () => async (dispatch) => {
  try {
    dispatch({ type: types.FETCH_VALIDATE_WPS })
    const resValidateWps = await http.get(
      `/api/v1/file/validate/bulk/wps`,
      {
        withCredentials: true
      }
    );
    if (checkSuccessStatus(resValidateWps.status)) {
      dispatch({
        type: types.FETCH_VALIDATE_WPS_SUCCESS,
        payload: resValidateWps.data
      })
    } else {
      dispatch({ type: types.FETCH_VALIDATE_WPS_FAILED })
    }
  } catch (error) {
    console.log("Error catch block here", error)
    dispatch({ type: types.FETCH_VALIDATE_WPS_FAILED })
  }
}

export const getAuthBulkPayments = () => async (dispatch) => {
  try {
    dispatch({ type: types.FETCH_AUTHORIZE_BULK_PAYMENT })
    const resAuthBulk = await http.get(
      `/api/v1/file/authorize/bulk`,
      {
        withCredentials: true
      }
    );
    if (checkSuccessStatus(resAuthBulk.status)) {
      dispatch({
        type: types.FETCH_AUTHORIZE_BULK_PAYMENT_SUCCESS,
        payload: resAuthBulk.data
      })
    } else {
      dispatch({ type: types.FETCH_AUTHORIZE_BULK_PAYMENT_FAILED })
    }
  } catch (error) {
    console.log("Error catch block here", error)
    dispatch({ type: types.FETCH_AUTHORIZE_BULK_PAYMENT_FAILED })
  }
}

export const getAuthIndividualPayments = () => async (dispatch) => {
  try {
    dispatch({ type: types.FETCH_AUTHORIZE_INDIVIDUAL_PAYMENT })
    const resAuthBulk = await http.get(
      `/api/v1/file/authorize/individual`,
      {
        withCredentials: true
      }
    );
    if (checkSuccessStatus(resAuthBulk.status)) {
      dispatch({
        type: types.FETCH_AUTHORIZE_INDIVIDUAL_PAYMENT_SUCCESS,
        payload: resAuthBulk.data
      })
    } else {
      dispatch({ type: types.FETCH_AUTHORIZE_INDIVIDUAL_PAYMENT_FAILED })
    }
  } catch (error) {
    console.log("Error catch block here", error)
    dispatch({ type: types.FETCH_AUTHORIZE_INDIVIDUAL_PAYMENT_FAILED })
  }
}

export const getAuthWpsPayments = () => async (dispatch) => {
  try {
    dispatch({ type: types.FETCH_AUTHORIZE_WPS_PAYMENT })
    const resAuthWps = await http.get(
      `/api/v1/file/authorize/bulk/wps`,
      {
        withCredentials: true
      }
    );
    if (checkSuccessStatus(resAuthWps.status)) {
      dispatch({
        type: types.FETCH_AUTHORIZE_WPS_PAYMENT_SUCCESS,
        payload: resAuthWps.data
      })

    } else {
      dispatch({ type: types.FETCH_AUTHORIZE_WPS_PAYMENT_FAILED })
    }
  } catch (error) {
    console.log("Error catch block here", error)
    dispatch({ type: types.FETCH_AUTHORIZE_WPS_PAYMENT_FAILED })
  }
}

export const validateValidBulkPayment = (fileID) => async (dispatch) => {

  try {
    const resValidateValidBulkPayment = await http.post(
      `/api/v1/file/${fileID}/validateBulk`,

      {
        withCredentials: true,
      }
    );

    if (checkSuccessStatus(resValidateValidBulkPayment.status)) {
      dispatch({
        type: types.VALIDATE_VALID_BULK_PAYMENT_SUCCESS,
        payload: resValidateValidBulkPayment.data
      });
      dispatch(getValidateBulkPayment())
    } else {
      dispatch({ type: types.VALIDATE_VALID_BULK_PAYMENT_FAILED })
    }
  } catch (error) {
    messages.error(error);
    console.log("Error catch block here", error);
  }
};

export const rejectValidateBulkPayment = (fileID) => async (dispatch) => {

  try {
    const resRejectValidBulkPayment = await http.post(
      `/api/v1/file/${fileID}/rejectBulk`,

      {
        withCredentials: true,
      }
    );

    if (checkSuccessStatus(resRejectValidBulkPayment.status)) {
      dispatch({
        type: types.REJECT_VALIDATE_BULK_PAYMENT_SUCCESS,
        payload: resRejectValidBulkPayment.data
      });
      dispatch(getValidateBulkPayment())
    } else {
      dispatch({ type: types.REJECT_VALIDATE_BULK_PAYMENT_FAILED })
    }
  } catch (error) {
    messages.error(error);
    console.log("Error catch block here", error);
  }
};

export const validValidateIndividualPayment = (fileId) => async (dispatch) => {
  try {
    const resValidateValidIndividualPayment = await http.post(
      `/api/v1/file/${fileId}/validateIndividual`,

      {
        withCredentials: true,
      }
    );

    if (checkSuccessStatus(resValidateValidIndividualPayment.status)) {
      dispatch({
        type: types.VALIDATE_VALID_INDIVIDUAL_PAYMENT_SUCCESS,
        payload: resValidateValidIndividualPayment.data
      });
      dispatch(getValidateIndividualPayment())
    } else {
      dispatch({ type: types.VALIDATE_VALID_INDIVIDUAL_PAYMENT_FAILED })
    }
  } catch (error) {
    messages.error(error);
    console.log("Error catch block here", error);
  }
};

export const rejectValidateIndividualPayment = (fileId) => async (dispatch) => {

  try {
    const resRejectValidBulkPayment = await http.post(
      `/api/v1/file/${fileId}/rejectIndividual`,

      {
        withCredentials: true,
      }
    );

    if (checkSuccessStatus(resRejectValidBulkPayment.status)) {
      dispatch({
        type: types.REJECT_VALIDATE_INDIVIDUAL_PAYMENT_SUCCESS,
        payload: resRejectValidBulkPayment.data
      });
      dispatch(getValidateIndividualPayment())
    } else {
      dispatch({ type: types.REJECT_VALIDATE_INDIVIDUAL_PAYMENT_FAILED})
    }
  } catch (error) {
    messages.error(error);
    console.log("Error catch block here", error);
  }
};

export const rejectAuthorizePayment = (fileId) => async (dispatch) => {

  try {
    const resRejectAuthorizePayment = await http.post(
      `/api/v1/file/${fileId}/rejectBulk`,

      {
        withCredentials: true,
      }
    );

    if (checkSuccessStatus(resRejectAuthorizePayment.status)) {
      dispatch({
        type: types.REJECT_AUTHORIZE_BULK_PAYMENT_SUCCESS,
        payload: resRejectAuthorizePayment.data
      });
      dispatch(getAuthBulkPayments())
    } else {
      dispatch({ type: types.REJECT_AUTHORIZE_BULK_PAYMENT_FAILED})
    }
  } catch (error) {
    messages.error(error);
    console.log("Error catch block here", error);
  }
};

export const rejectAuthorizeIndividualPayment = (fileID) => async (dispatch) => {

  try {
    const resRejectAuthorizeInPayment = await http.post(
      `/api/v1/file/${fileID}/rejectIndividual`,

      {
        withCredentials: true,
      }
    );

    if (checkSuccessStatus(resRejectAuthorizeInPayment.status)) {
      dispatch({
        type: types.REJECT_AUTHORIZE_INDIVIDUAL_PAYMENT_SUCCESS,
        payload: resRejectAuthorizeInPayment.data
      });
      dispatch(getAuthIndividualPayments())
    } else {
      dispatch({ type: types.REJECT_AUTHORIZE_INDIVIDUAL_PAYMENT_FAILED})
    }
  } catch (error) {
    messages.error(error);
    console.log("Error catch block here", error);
  }
};

export const getBalance = () => async (dispatch) => {
  try {
    dispatch({ type: types.FETCH_BALANCE })
    const balanceResponse = await http.get(
      `/api/v1/transaction/balance/portal`,
      {
        withCredentials: true
      }
    );
    if (checkSuccessStatus(balanceResponse.status)) {
      dispatch({
        type: types.FETCH_BALANCE_SUCCESS,
        payload: balanceResponse.data
      })

    } else {
      dispatch({ type: types.FETCH_BALANCE_FAILED  })
    }
  } catch (error) {
    console.log("Error catch block here", error)
    dispatch({ type: types.FETCH_BALANCE_FAILED })
  }
};

export const approveAuthBulkPayment = (approveFileID) => async (dispatch) => {

  try {
    const resApproveAuthBulkPayment = await http.post(
      `/api/v1/file/${approveFileID}/bulk`,

      {
        withCredentials: true,
      }
    );

    if (checkSuccessStatus(resApproveAuthBulkPayment.status)) {
      dispatch({
        type: types.APPROVE_AUTHORIZE_BULK_PAYMENT_SUCCESS,
        payload: resApproveAuthBulkPayment.data
      });
      dispatch(getAuthBulkPayments())
    } else {
      dispatch({ type: types.APPROVE_AUTHORIZE_BULK_PAYMENT_FAILED })
    }
  } catch (error) {
    messages.error(error);
    console.log("Error catch block here", error);
  }
};

export const approveAuthIndividualPayment = (approveFileId) => async (dispatch) => {

  try {
    const resApproveAuthIndiPayment = await http.post(
      `/api/v1/file/${approveFileId}/individual`,

      {
        withCredentials: true,
      }
    );
    
    if (checkSuccessStatus(resApproveAuthIndiPayment.status)) {
      dispatch({
        type: types.APPROVE_AUTHORIZE_INDIVIDUAL_PAYMENT_SUCCESS,
        payload: resApproveAuthIndiPayment.data
      });
      dispatch(getAuthIndividualPayments())
    } else {
      dispatch({ type: types.APPROVE_AUTHORIZE_INDIVIDUAL_PAYMENT_FAILED })
    }
  } catch (error) {
    messages.error(error);
    console.log("Error catch block here", error);
  }
};

export const getDownloadFile = (downloadFileId) => async (dispatch) => {
  try {
    dispatch({ type: types.FETCH_DOWNLOAD_FILE })
    const downloadFileResponse = await http.get(
      `fileDownload?fileId=${downloadFileId}`,
      {
        withCredentials: true
      }
    );
    if (checkSuccessStatus(downloadFileResponse.status)) {
      dispatch({
        type: types.FETCH_DOWNLOAD_FILE_SUCCESS,
        payload: downloadFileResponse.data
      })

    } else {
      dispatch({ type: types.FETCH_DOWNLOAD_FILE_FAILED   })
    }
  } catch (error) {
    console.log("Error catch block here", error)
    dispatch({ type: types.FETCH_DOWNLOAD_FILE_FAILED })
  }
}

