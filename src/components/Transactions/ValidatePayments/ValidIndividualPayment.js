import React, { useEffect } from "react";
import {Table} from "reactstrap";
import { Download, Check, X } from "react-feather";
import {
  getValidateIndividualPayment,
  validValidateIndividualPayment,
  rejectValidateIndividualPayment,
  getBalance,
  getDownloadFile,
} from "../../../actions/createAllBulkPaymentAction";
import { useDispatch, useSelector } from "react-redux";

function ValidIndividualPayment(props) {

  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getValidateIndividualPayment());
    dispatch(getBalance());
  }, [dispatch]);

  const validIndividualData = useSelector(
    (state) => state.transactionReducer.validateIndividualPayment.transactions
  );
  const balance = useSelector((state) => state.transactionReducer.balance);

  function submitValidateData(individual) {
    const fileId = individual._id;
    alert("Are you sure you want to validate this payment?");
    dispatch(validValidateIndividualPayment(fileId));
  }

  function rejectValidateData(individual) {
    const fileId = individual._id;
    alert("Are you sure you want to reject this payment?");
    dispatch(rejectValidateIndividualPayment(fileId));
  }

  function downloadFile(individual) {
    const downloadFileId = individual._id;
    dispatch(getDownloadFile(downloadFileId));
  }

  return (
    <div>
      <h6>Total balance held with NOW Money:AED {balance}</h6>

      <Table responsive className="table-style-transaction">
        <thead>
          <tr>
            <th>Emirates ID</th>
            <th>Type</th>
            <th>Employee name</th>
            <th>Amount</th>
            <th>Description</th>
            <th>Status</th>
            <th>Createted at</th>
            <th>Updated at</th>
            <th>Schduled For</th>
            <th> Download</th>
            <th>Validate</th>
            <th>Reject</th>
          </tr>
        </thead>
        <tbody>
          {validIndividualData?.map((individual, i) => {
            return (
              <tr key={i}>
                <td>{individual.individualSalaryFileEid || "N/A"}</td>
                <td>{individual.fileType || "N/A"}</td>
                <td>{individual.userFileName || "N/A"}</td> 
                <td>{individual.amount || "N/A"}</td>
                <td>{individual.description || "N/A"}</td>
                <td>{individual.status || "N/A"}</td>
                <td>{individual.uploadedAt || "N/A"}</td>
                <td>{individual.statusUpdatedAt || "N/A"}</td>
                <td>{individual.scheduledFor || "ASAP"}</td>
                <td className="table-text-center">
                  <a onClick={() => downloadFile(individual)}>
                    <Download size={18} />
                  </a>
                </td>
                <td className="table-text-center">
                  <a onClick={() => submitValidateData(individual)}>
                    <Check size={18} />
                  </a>
                </td>
                <td className="table-text-center">
                  <a onClick={() => rejectValidateData(individual)}>
                    <X size={18} />
                  </a>
                </td>
              </tr>
            );
          })}
        </tbody>
      </Table>
    </div>
  );
}

export default ValidIndividualPayment;
