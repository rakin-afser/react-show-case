import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import {Table} from "reactstrap";

import {
  getValidateWpsPayments,
  getBalance,
} from "../../../actions/createAllBulkPaymentAction";

function ValidWpsPayment(props) {
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(getValidateWpsPayments());
    dispatch(getBalance());
  }, []);

  const validateWpsPayments = useSelector(
    (state) => state.transactionReducer.validateWpsPayment.transactions
  );
  const balance = useSelector((state) => state.transactionReducer.balance);

  return (
    <div>
      <h6>Total balance held with NOW Money:AED {balance}</h6>
      <Table responsive className="table-style-transaction">
        <thead>
          <tr>
            <th>Payment ID</th>
            <th>File Name</th>
            <th>Amount</th>
            <th>Fees</th>
            <th>Status</th>
            <th>Createted at</th>
            <th>Updated at</th>
            <th> Download</th>
            <th>Validate</th>
            <th>Reject</th>
          </tr>
        </thead>
        <tbody></tbody>
      </Table>
    </div>
  );
}

export default ValidWpsPayment;
