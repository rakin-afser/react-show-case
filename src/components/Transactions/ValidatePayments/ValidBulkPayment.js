import React, { useEffect } from "react";
import { Table } from "reactstrap";
import { Download, Check, X } from "react-feather";
import { useDispatch, useSelector } from "react-redux";
import {
  getValidateBulkPayment,
  validateValidBulkPayment,
  rejectValidateBulkPayment,
  getBalance,
  getDownloadFile,
} from "../../../actions/createAllBulkPaymentAction";

function ValidBulkPayment(props) {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getValidateBulkPayment());
    dispatch(getBalance());
  }, []);

  const validBulkPayments = useSelector(
    (state) => state.transactionReducer.validateBulkPayment.transactions
  );
  const balance = useSelector((state) => state.transactionReducer.balance);

  function submitValidateData(validBulkPayment) {
    const fileID = validBulkPayment._id;
    alert("Are you sure you want to validate this payment?");
    dispatch(validateValidBulkPayment(fileID));
  }

  function rejectValidateData(validBulkPayment) {
    const fileID = validBulkPayment._id;
    alert("Are you sure you want to reject this payment?");
    dispatch(rejectValidateBulkPayment(fileID));
  }

  function downloadFile(validBulkPayment) {
    const downloadFileId = validBulkPayment._id;
    dispatch(getDownloadFile(downloadFileId));
  }

  return (
    <div>
      <h6>Total balance held with NOW Money:AED {balance}</h6>

      <Table responsive className="table-style-transaction">
        <thead>
          <tr>
            <th>Payment ID</th>
            <th>File Name</th>
            <th>Amount</th>
            <th>Status</th>
            <th>Createted at</th>
            <th>Updated at</th>
            <th>Schduled For</th>
            <th> Download</th>
            <th>Validate</th>
            <th>Reject</th>
          </tr>
        </thead>
        <tbody>
          {validBulkPayments?.map((validBulkPayment, i) => {
            return (
              <tr key={i}>
                <td>{validBulkPayment._id || "N/A"}</td>
                <td>{validBulkPayment.userFileName || "N/A"}</td>
                <td>{validBulkPayment.amount || "N/A"}</td>
                <td>{validBulkPayment.status || "N/A"}</td>
                <td>{validBulkPayment.uploadedAt || "N/A"}</td>
                <td>{validBulkPayment.statusUpdatedAt || "N/A"}</td>
                <td>{validBulkPayment.scheduledFor || "ASAP"}</td>
                <td className="table-text-center">
                  <a onClick={() => downloadFile(validBulkPayment)}>
                    <Download size={18} />
                  </a>
                </td>
                <td className="table-text-center">
                  <a onClick={() => submitValidateData(validBulkPayment)}>
                    <Check size={18} />
                  </a>
                </td>
                <td className="table-text-center">
                  <a onClick={() => rejectValidateData(validBulkPayment)}>
                    <X size={18} />
                  </a>
                </td>
              </tr>
            );
          })}
        </tbody>
      </Table>
    </div>
  );
}

export default ValidBulkPayment;
