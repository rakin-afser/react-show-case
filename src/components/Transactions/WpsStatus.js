import React, { useState } from "react";
import {
    Table,
    Button,
    ButtonGroup,
    UncontrolledDropdown,
    DropdownToggle,
    DropdownMenu,
    DropdownItem,
    Input,
} from "reactstrap";

function WpsStatus(props) {
    const [paymentId, setPaymentId] = useState(true);
    const [fileName, setFileName] = useState(true);
    const [amount, setAmount] = useState(true);
    const [fees, setFees] = useState(true);
    const [status, setStatus] = useState(true);
    const [createdAt, setCreateAt] = useState(true);
    const [updatedAt, setUpdatedAt] = useState(true);
    const [download, setDownload] = useState(true)
    function displayAll() {
        if (paymentId === false) {
            setPaymentId(!paymentId)
        }
        if (fileName === false) {
            setFileName(!fileName)
        }
        if (amount === false) {
            setAmount(!amount)
        }
        if (fees === false) {
            setFees(!fees)
        }
        if (status === false) {
            setStatus(!status)
        }
        if (createdAt === false) {
            setCreateAt(!createdAt)
        }
        if (updatedAt === false) {
            setUpdatedAt(!updatedAt)
        }
        if (download === false) {
            setDownload(!download)
        }

    }
    function renderColumn({ label, onChange, selected }) {
        return (
            <DropdownItem>
                <Input type="checkbox" id="checkbox1" checked={selected} onChange={() => onChange(!selected)} />
                <span className="text-gray">{label}</span>
            </DropdownItem>
        )
    }
    return (
        <div>
            <h6>Total balance held with NOW Money:AED</h6>
            <div className="ml-auto float-right">
                <ButtonGroup>
                    <Button onClick={displayAll}>Display all</Button>
                    <UncontrolledDropdown inNavbar className="pr-1">
                        <DropdownToggle >
                            Display
                </DropdownToggle>
                        <DropdownMenu right>
                            {renderColumn({ label: "Payment ID", onChange: setPaymentId, selected: paymentId })}
                            {renderColumn({ label: "File Name", onChange: setFileName, selected: fileName })}
                            {renderColumn({ label: " Amount", onChange: setAmount, selected: amount })}
                            {renderColumn({ label: " Fees", onChange: setFees, selected: fees })}
                            {renderColumn({ label: " Status", onChange: setStatus, selected: status })}
                            {renderColumn({ label: " Createted at", onChange: setCreateAt, selected: createdAt })}
                            {renderColumn({ label: " Updated at", onChange: setUpdatedAt, selected: updatedAt })}
                            {renderColumn({ label: " Download", onChange: setDownload, selected: download })}

                        </DropdownMenu>
                    </UncontrolledDropdown>
                </ButtonGroup>
            </div>
            <Table responsive>
                <thead>
                    <tr >
                        {paymentId ? <th >Payment ID</th> : null}
                        {fileName ? <th>File Name</th> : null}
                        {amount ? <th >Amount</th> : null}
                        {fees ? <th >Fees</th> : null}
                        {status ? <th >Status</th> : null}
                        {createdAt ? <th >Createted at</th> : null}
                        {updatedAt ? <th>Updated at</th> : null}
                        {download ? <th > Download</th> : null}

                    </tr>
                </thead>
                <tbody>

                    {/* {data?.employees?.map((item, i) => {
            return (
              <tr key={i}>
                {employeeName ? <td>{item.otherNames + " " + item.lastName || "-"}</td> : null}
                {idCardNumber ? <td>{item.idCardNumber}</td> : null}
                {iban ? <td>{item.wpsIban || "***"}</td> : null}
                {refId ? <td>{item.refId || "-"}</td> : null}
                {position ? <td>{item.position || "-"}</td> : null}
                {department ? <td>{item.department || "-"}</td> : null}
                {createdAt ? <td>{item.createdAt || "-"}</td> : null}
                {status ? <td>{item.status || "-"}</td> : null}
                {remove ? <td><Edit size={18} className="mr-2" /> <Trash2 size={18} color="#FF586B" /></td> : null}
              </tr>
            )
          })} */}

                </tbody>
            </Table>
        </div>
    );
}

export default WpsStatus;