import React, { useState } from "react";
import {
    Table,
    Button,
    ButtonGroup,
    UncontrolledDropdown,
    DropdownToggle,
    DropdownMenu,
    DropdownItem,
    Input,
} from "reactstrap";

function ViewPayments(props) {

    const [employeeName, setEmployeeName] = useState(true);
    const [idCardNumber, setIdCardNumber] = useState(true);
    const [type, setType] = useState(true);
    const [description, setDescription] = useState(true);
    const [date, setDate] = useState(true);
    const [amount, setAmount] = useState(true);
    const [status, setStatus] = useState(true);
    const [paymentId, setPaymentId] = useState(true)

    function displayAll() {
        if (employeeName === false) {
            setEmployeeName(!employeeName)
        }
        if (idCardNumber === false) {
            setIdCardNumber(!idCardNumber)
        }
        if (type === false) {
            setType(!type)
        }
        if (description === false) {
            setDescription(!description)
        }
        if (date === false) {
            setDate(!date)
        }
        if (amount === false) {
            setAmount(!amount)
        }
        if (status === false) {
            setStatus(!status)
        }
        if (paymentId === false) {
            setPaymentId(!paymentId)
        }
    }

    function renderColumn({ label, onChange, selected }) {
        return (
            <DropdownItem>
                <Input type="checkbox" id="checkbox1" checked={selected} onChange={() => onChange(!selected)} />
                <span className="text-gray">{label}</span>
            </DropdownItem>
        )
    }
    return (
        <div>
            <div className="ml-auto float-right">
                <ButtonGroup>
                    <Button onClick={displayAll}>Display all</Button>
                    <UncontrolledDropdown inNavbar className="pr-1">
                        <DropdownToggle >
                            Display
                </DropdownToggle>
                        <DropdownMenu right>
                            {renderColumn({ label: "Employee Name", onChange: setEmployeeName, selected: employeeName })}
                            {renderColumn({ label: " Emirates ID", onChange: setIdCardNumber, selected: idCardNumber })}
                            {renderColumn({ label: " Type", onChange: setType, selected: type })}
                            {renderColumn({ label: " Description", onChange: setDescription, selected: description })}
                            {renderColumn({ label: " Date", onChange: setDate, selected: date })}
                            {renderColumn({ label: " Amount", onChange: setAmount, selected: amount })}
                            {renderColumn({ label: " Status", onChange: setStatus, selected: status })}
                            {renderColumn({ label: " Payment ID", onChange: setPaymentId, selected: paymentId })}
                        </DropdownMenu>
                    </UncontrolledDropdown>
                </ButtonGroup>
            </div>

            <Table responsive>
                <thead>
                    <tr >
                        {employeeName ? <th >Employee name</th> : null}
                        {idCardNumber ? <th>Emirates ID</th> : null}
                        {type ? <th>Type</th> : null}
                        {description ? <th >Description</th> : null}
                        {date ? <th >Date</th> : null}
                        {amount ? <th >Amount</th> : null}
                        {status ? <th >Status</th> : null}
                        {paymentId ? <th >Payment ID</th> : null}
                    </tr>
                </thead>
                <tbody>

                    {/* {data?.employees?.map((item, i) => {
            return (
              <tr key={i}>
                {employeeName ? <td>{item.otherNames + " " + item.lastName || "-"}</td> : null}
                {idCardNumber ? <td>{item.idCardNumber}</td> : null}
                {iban ? <td>{item.wpsIban || "***"}</td> : null}
                {refId ? <td>{item.refId || "-"}</td> : null}
                {position ? <td>{item.position || "-"}</td> : null}
                {department ? <td>{item.department || "-"}</td> : null}
                {createdAt ? <td>{item.createdAt || "-"}</td> : null}
                {status ? <td>{item.status || "-"}</td> : null}
                {remove ? <td><Edit size={18} className="mr-2" /> <Trash2 size={18} color="#FF586B" /></td> : null}
              </tr>
            )
          })} */}

                </tbody>
            </Table>
        </div>
    );
}

export default ViewPayments;