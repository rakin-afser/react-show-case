import React, { Fragment } from "react";
import { Button, Form, FormGroup, Label, Input, Col, Row, CardBody, Card, } from 'reactstrap';
import {
  Upload, UploadCloud

} from "react-feather"
import styled from "styled-components";
import UploadData from "rc-upload";
const Container = styled.div`
  flex: 1;
  display: flex;
  flex-direction: column;
  align-items: center;
  padding: 20px;
  border-width: 2px;
  border-radius: 2px;
  border-style: dashed;
  border-color: #FCE6D5;
  color: white;
  color: #FCE6D5;
  outline: none;
  transition: border 0.24s ease-in-out;
`;
function UploadTransferProofs(props) {

  return (
    <Fragment>
      <Card>
        <CardBody>

          <h4>NOW Money account details</h4>

          <Label sm={2}>Bank name : </Label>COMMERCIAL BANK OF DUBAI Noor Bank<br />
          <Label sm={2}>Bank address : </Label>Main Branch, Dubai, UAE P.O. BOX 8822, JLT BRANCH, DUBAI, UAE<br />
          <Label sm={2}>SWIFT : </Label>CBDUAEAD NISLAEAD <br />
          <Label sm={2}>Account name : </Label>NOW PAYMENT SERVICES PROVIDER LLC N O W PAYMENT SERVICES PROVIDER L L C<br />
          <Label sm={2}>Account number : </Label>1002489464 02410908520034<br />
          <Label sm={2}>IBAN : </Label> AE650230000001002489464 AE420520002410908520034<br />
          <i><small>*Please note the above account details accept only transfers made online, on the phone or in branch. For cash or cheque deposits, please contact NOW Money to make arrangements</small></i>
          <br /><br />
          <h4>Upload deposit proof</h4>
          <p>When you make a transfer to NOW Money, if you have any form of proof of transfer you can upload it here which may allow us to track your transfer and credit your balance sooner</p>
          <Form className="deposit-proof-form">
            <FormGroup className="form-group row">
              <Label htmlFor="deposit-proof" className="col-sm-2  col-form-Label"
              >Deposit proof</Label
              >
              <Col className="col-sm-10">
                <Container className="hover-color">
                  <UploadCloud/>
                <Input
                  data-height="100"
                  id="deposit-proof"
                  className="dropify deposit-proof-file"
                  type="file"
                  accept=""
                  data-parsley-trigger="change"
                  required=""
                />
                <Label>Drag 'n' drop some files here, or click to select files</Label>
                </Container>
              </Col>
            </FormGroup>
            <FormGroup className="form-group row">
              <Label htmlFor="employee-file" className="col-sm-2  col-form-Label"
              >Deposit value (in AED)</Label
              >
              <Col className="col-sm-10">

                <Input
                  data-height="100"
                  id="deposit-value"
                  className="form-control"
                  data-parsley-pattern="^(\d*\.?\d{2}|\d{1,3}(,\d{3})*(\.\d{2})?)$"
                  data-parsley-trigger="change"
                  required=""
                />
              </Col>
            </FormGroup>
            <Button
            block
              id="depositProofUpload"
              type="submit"
              color="primary"
            >
              <Upload />Upload
            </Button>
          </Form>

        </CardBody>
      </Card>
    </Fragment >
  );
}
export default UploadTransferProofs