
import React, { Fragment,useState } from "react";
import { Button, Form, FormGroup, Label, Input, Col, Row, CardBody, Card,UncontrolledDropdown,
  DropdownToggle, DropdownMenu, DropdownItem } from 'reactstrap';
import { CheckSquare, X,UploadCloud,Download} from "react-feather";
import styled from "styled-components";
import UploadData from "rc-upload";

const Container = styled.div`
  flex: 1;
  display: flex;
  flex-direction: column;
  align-items: center;
  padding: 20px;
  border-width: 2px;
  border-radius: 2px;
  border-style: dashed;
  border-color: #FCE6D5;
  color: white;
  color: #FCE6D5;
  outline: none;
  transition: border 0.24s ease-in-out;
`;


function CreateWPSDisbursementPayments(props) {

  const [readyForUploadFM,setreadyForUploadFM] = useState(null)
 
  const uploaderFMProps = {
    accept: "csv/*",
    data: { test: "toto" },
    multiple: false,
    onStart: () => {
      console.log(">>> onStart");
    },
    onSuccess: () => {
      console.log(">>> onSuccess");
    },
    onError: (err, responce, file) => {
      console.log("onError", err);
    },
    beforeUpload: file => {
      console.log(">>> beforeUpload", file);
    }
  };
  const handleChange = () => {

  }

  const handleSubmit = (e) => {

  }

  return (
    <div>
     <Row>
        <Col xl="11">
        <h6>
        Total balance held with NOW Money: AED
        </h6>
        </Col>
     
        <Col xl="1">
        <UncontrolledDropdown>
        <DropdownToggle nav caret>
          <Download size={18} />
        </DropdownToggle>
        <DropdownMenu right>
          <DropdownItem>
        <a href="/paymentFile" target="_blank"><Download size={18} />Download File</a>
        </DropdownItem>
        </DropdownMenu>
      </UncontrolledDropdown></Col>
      </Row>
      <Card>
        <CardBody>
          <h6>
            Upload WPS salary file
                    </h6>
          <Form onSubmit={handleSubmit}>
            <FormGroup row>
              <Col sm="12">
                <Label for="salaryFile"
                >Salary file</Label
                >

               
                  <UploadData {...uploaderFMProps}>
                  <Container className="hover-color">
                  <UploadCloud/>
                  <Label>Drag 'n' drop some files here, or click to select files</Label>
                  </Container>
                   
                  </UploadData>

               
              </Col>
            </FormGroup>

            <FormGroup row>
              <Col sm="12">
                <Label
                  for="totalAmount"
                >Total amount (AED)</Label
                >
                <Input
                  type="number"
                  name="totalAmount"
                  onChange={handleChange}
                  placeholder="0.00"
                  required
                />
              </Col>
            </FormGroup>

            <FormGroup row>
              <Col sm="12">
                <Label
                  for="numOfRecords"
                >Number of employees</Label
                >
                <Input
                  type="number"
                  onChange={handleChange}
                  name="numOfRecords"
                  placeholder="0.00"
                  required
                />
              </Col>
            </FormGroup>
            <br />
            <div className="float-right">
              <div className="float-left">
                <Button color="danger" className="mr-1">
                  <X size={16} color="#FFF" />
                         Cancel
                      </Button>
              </div>
              <Button
                id="wpsSalaryUpload"
                type="submit"
                color="primary"
              >
                <CheckSquare />Save
                      </Button>
            </div>
          </Form>
        </CardBody>
      </Card>
    </div>

  );
}
export default CreateWPSDisbursementPayments;