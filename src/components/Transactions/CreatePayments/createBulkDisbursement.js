import React, { Fragment, useState } from "react";
import {
  Button,
  Form,
  FormGroup,
  Label,
  Input,
  Col,
  Card,
  CardBody,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  Row,
  Progress,
} from "reactstrap";
import styled from "styled-components";
import { CheckSquare, X, UploadCloud, Download } from "react-feather";
import UploadData from "rc-upload";
import { useDispatch } from "react-redux";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import moment from "moment";
import { createBulkPayment } from "../../../actions/createAllBulkPaymentAction";
const Container = styled.div`
  flex: 1;
  display: flex;
  flex-direction: column;
  align-items: center;
  padding: 15px;
  border-width: 2px;
  border-radius: 2px;
  border-style: dashed;
  border-color: #fce6d5;
  color: white;
  outline: none;
  transition: border 0.24s ease-in-out;
`;

function CreateBulkDisbursement(props) {
  const [scheduledFor, setscheduledFor] = useState(null);
  const [startDate, setStartDate] = useState(new Date());
  const [showPayAt, setShowPayAt] = useState(false);
  const [defaultFileList, setDefaultFileList] = useState([]);
  const [progress, setProgress] = useState(0);
  const dispatch = useDispatch();

  const dateTimes = moment(startDate).format("y-mm-DD HH:mm");
  
  const uploadFiles = (options) => {
    
    const { onSuccess, onError, file, onProgress } = options;

    const fmData = new FormData();
    const config = {
      headers: { "content-type": "multipart/form-data" },
      onUploadProgress: (event) => {
        const percent = Math.floor((event.loaded / event.total) * 100);
        setProgress(percent);
        if (percent === 100) {
          setTimeout(() => setProgress(0), 1000);
        }
        // onProgress({ percent: (event.loaded / event.total) * 100 });
      },
    };
    fmData.append("file", file);
   
    fmData.append("fileName", file.name);
    dispatch(createBulkPayment(fmData, config));
  };

  const handleOnChange = (e) => {
    setDefaultFileList(e.target.files[0]);
  };
  return (
    <div>
      <Row>
        <Col xl="11">
          <h6>Total balance held with NOW Money: AED</h6>
        </Col>

        <Col xl="1">
          <UncontrolledDropdown>
            <DropdownToggle nav caret>
            <Download size={18} />
            </DropdownToggle>
            <DropdownMenu right>
              <DropdownItem>
              <a href="/paymentFile" target="_blank"><Download size={18} />Download file</a>
                
              </DropdownItem>
            </DropdownMenu>
          </UncontrolledDropdown>
        </Col>
      </Row>

      <Card>
        <CardBody>
          <h6>Upload salary file with Emirates ID and salary amount</h6>
          <Form onSubmit={uploadFiles}>
            <FormGroup row>
              <Col sm={12}>
                <Label for="salaryFile">Salary file</Label>

                <Container className="hover-color">
                  <UploadCloud />
                  <UploadData
                    accept="csv/*"
                    type="file"
                    onChange={handleOnChange}
                    customRequest={uploadFiles}
                  >
                    {defaultFileList.length >= 1 ? null : (
                      <Label>
                        Drag 'n' drop some files here, or click to select files
                      </Label>
                    )}
                  </UploadData>
                  {progress > 0 ? <Progress percent={progress} /> : null}
                </Container>
              </Col>
            </FormGroup>
            <FormGroup row>
              <Col sm={12}>
                <Label for="scheduledFor">Payment time</Label>
                <Input
                  id="scheduledFor"
                  type="select"
                  name="scheduledFor"
                  onClick={() => setShowPayAt(!showPayAt)}
                  onChange={(e) => setscheduledFor(e.target.value)}
                >
                  <option value="ASAP">As soon as possible</option>
                  <option value={dateTimes}>Schedule</option>
                </Input>
              </Col>
            </FormGroup>
            {showPayAt === true ? (
              <FormGroup row>
                <Col sm={12}>
                  <Label for="scheduledFor">Pay At : </Label>
                  <DatePicker
                    selected={startDate}
                    onChange={(dateTime) => {
                      setStartDate(dateTime);
                    }}
                    showTimeSelect
                    dateFormat="y-mm-dd h:mm aa"
                    isClearable
                  />
                </Col>
              </FormGroup>
            ) : null}
            <br />
            <div className="float-right">
              <div className="float-left">
                <Button color="danger" className="mr-1">
                  <X size={16} color="#FFF" /> Cancel
                </Button>
              </div>
              <Button id="createDisbursement" type="submit" color="primary">
                <span>
                  <CheckSquare />
                  Save{" "}
                </span>
              </Button>
            </div>
          </Form>
        </CardBody>
      </Card>
    </div>
  );
}
export default CreateBulkDisbursement;
