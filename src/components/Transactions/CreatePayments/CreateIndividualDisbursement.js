import React, { Fragment, useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  Button,
  Form,
  FormGroup,
  Label,
  Input,
  CardBody,
  Col,
  Row,
  Container,
  Card,
} from "reactstrap";
import { CheckSquare, X } from "react-feather";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import moment from "moment";
import { createIndividualPayment } from "../../../actions/createAllBulkPaymentAction";
import { getEmployeesData, getSearch } from "../../../actions/employeeActions";
function CreateIndividualPayments(props) {
  const [showPayAt, setShowPayAt] = useState(false);
  const [scheduledFor, setscheduledFor] = useState(new Date());
  const [createIndividualData, setcreateIndividualData] = useState({
    name: "",
    id: "",
    salary: "",
    paymentType: "",
    description: "",
  });
  const data = useSelector((state) => state.appReducer.employeesData.employees);
  const dispatch = useDispatch();
  const { name, id, salary, paymentType, description } = createIndividualData;
  function handleChange(e) {
    setcreateIndividualData({
      ...createIndividualData,
      [e.target.name]: e.target.value,
    });
  }

  const dateTimes = moment(scheduledFor).format("y-mm-DD HH:mm");
  useEffect(() => {
    dispatch(getEmployeesData());
  }, [dispatch]);

  const handleSubmit = (e) => {
    e.preventDefault();
    dispatch(
      createIndividualPayment(
        name,
        id,
        salary,
        paymentType,
        description,
        scheduledFor
      )
    );
  };
  return (
    <div>
      <Container>
        <h5> Total balance held with NOW Money: AED</h5>
        <Row>
          <Col sm="12">
            <Card>
              <CardBody>
                <Form onSubmit={handleSubmit}>
                  <p>Select employee to transfer create payment</p>
                  <Row>
                    <Col sm="3">
                      <Label for="employeeName">Benificiary name</Label>
                    </Col>
                    <Col sm="9">
                      <FormGroup>
                        <Input
                          className="selectpicker"
                          type="select"
                          id="search-term"
                          name="name"
                          className="border-primary"
                          onChange={handleChange}
                        >
                          {data?.map((name) => {
                            return (
                              <option key={name._id} value={name.lastName}>
                                {name.lastName}
                              </option>
                            );
                          })}
                        </Input>
                      </FormGroup>
                    </Col>
                  </Row>
                  <Row>
                    <Col sm="3">
                      <Label for="employeeEid">Emirates ID number</Label>
                    </Col>
                    <Col sm="9">
                      <FormGroup>
                        <Input
                          type="select"
                          className="border-primary"
                          name="id"
                          placeholder="7104XXXXXXXXXXXX"
                          onChange={handleChange}
                        >
                          {data?.map((emId) => {
                            return (
                              <option key={emId._id} value={emId.idCardNumber}>
                                {emId.idCardNumber}
                              </option>
                            );
                          })}
                        </Input>
                      </FormGroup>
                    </Col>
                  </Row>
                  <Row>
                    <Col sm="3">
                      <Label for="employeeSalary">Amount</Label>
                    </Col>
                    <Col sm="9">
                      <FormGroup>
                        <Input
                          type="number"
                          className="border-primary"
                          placeholder="Amount in AED"
                          name="salary"
                          required=""
                          onChange={handleChange}
                        />
                      </FormGroup>
                    </Col>
                  </Row>
                  <Row>
                    <Col sm="3">
                      <Label for="paymentType">Payment type</Label>
                    </Col>
                    <Col sm="9">
                      <FormGroup>
                        <Input
                          type="text"
                          className="border-primary"
                          name="paymentType"
                          placeholder="Payment type"
                          required=""
                          onChange={handleChange}
                        />
                      </FormGroup>
                    </Col>
                  </Row>
                  <Row>
                    <Col sm="3">
                      <Label for="description">Description</Label>
                    </Col>
                    <Col sm="9">
                      <FormGroup>
                        <Input
                          type="text"
                          className="border-primary"
                          placeholder="Description"
                          name="description"
                          onChange={handleChange}
                        />
                      </FormGroup>
                    </Col>
                  </Row>
                  <Row>
                    <Col sm="3">
                      <Label for="scheduledFor">Payment time</Label>
                    </Col>
                    <Col sm="9">
                      <FormGroup>

                        <Input type="select" name="scheduledFor" className="border-primary" onChange={(e) => {
                          setShowPayAt(!showPayAt)
                          handleChange(e)
                        }}
                        >
                          <option value="ASAP">As soon as possible</option>
                          <option value={dateTimes}>Schedule</option>
                        </Input>
                      </FormGroup>
                    </Col>
                  </Row>
                  {showPayAt === true ? (
                    <Row>
                      <Col sm="3">
                        <Label for="scheduledFor">Pay At</Label>
                      </Col>
                      <Col sm="9">
                        <FormGroup>
                          <DatePicker
                            selected={scheduledFor}
                            onChange={(t) => {
                              setscheduledFor(t);
                            }}
                            showTimeSelect
                            dateFormat="y-mm-dd HH:mm aa"
                            isClearable
                          />
                        </FormGroup>
                      </Col>
                    </Row>
                  ) : null}

                  <div className="float-right">
                    <div className="float-left">
                      <Button color="danger" className="mr-1">
                        <X size={16} color="#FFF" /> Cancel
                      </Button>
                    </div>
                    <Button color="primary" type="submit">
                      <CheckSquare size={16} color="#FFF" />
                      Save
                    </Button>
                  </div>
                </Form>
              </CardBody>
            </Card>
          </Col>
        </Row>
      </Container>
    </div>
  );
}
export default CreateIndividualPayments;
