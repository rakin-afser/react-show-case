import React, { useEffect } from "react";
import { Table } from "reactstrap";
import { useDispatch, useSelector } from "react-redux";
import { Download, Check, X } from "react-feather";
import {
  getAuthBulkPayments,
  getBalance,
  rejectAuthorizePayment,
  approveAuthBulkPayment,
  getDownloadFile,
} from "../../../actions/createAllBulkPaymentAction";

function AuthBulkPayment(props) {
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(getAuthBulkPayments());
    dispatch(getBalance());
  }, []);
  const authBulkPayments = useSelector(
    (state) => state.transactionReducer.authBulkPayment.transactions
  );
  const balance = useSelector((state) => state.transactionReducer.balance);

  function rejectAuthorizeBulk(authBulkPayment) {
    const fileId = authBulkPayment._id;
    alert("Are you sure you want to reject this payment?");
    dispatch(rejectAuthorizePayment(fileId));
  }

  function submitApprove(authBulkPayment) {
    const approveFileID = authBulkPayment._id;
    alert("Are you sure you want to approve this payment?");
    dispatch(approveAuthBulkPayment(approveFileID));
  }
  function downloadFile(authBulkPayment) {
    const downloadFileId = authBulkPayment._id;
    dispatch(getDownloadFile(downloadFileId));
  }

  return (
    <div>
      <h6>Total balance held with NOW Money:AED {balance}</h6>

      <Table responsive className="table-style-transaction">
        <thead>
          <tr>
            <th>Payment ID</th>
            <th>File Name</th>
            <th>Amount</th>
            <th>Status</th>
            <th>Createted at</th>
            <th>Updated at</th>
            <th>Schduled For</th>
            <th> Download</th>
            <th>Approve</th>
            <th>Reject</th>
          </tr>
        </thead>
        <tbody>
          {authBulkPayments?.map((authBulkPayment, i) => {
            return (
              <tr key={i}>
                <td>{authBulkPayment._id.toUpperCase() || "N/A"}</td>
                <td>{authBulkPayment.userFileName || "N/A"}</td>
                <td>{authBulkPayment.amount || "N/A"}</td>
                <td>{authBulkPayment.status || "N/A"}</td>
                <td>{authBulkPayment.uploadedAt || "N/A"}</td>
                <td>{authBulkPayment.statusUpdatedAt || "N/A"}</td>
                <td>{authBulkPayment.scheduledFor || "ASAP"}</td>
                <td className="table-text-center">
                  <a onClick={() => downloadFile(authBulkPayment)}>
                    <Download size={18} />
                  </a>
                </td>
                <td className="table-text-center">
                  <a onClick={() => submitApprove(authBulkPayment)}>
                    <Check size={18} />
                  </a>
                </td>
                <td className="table-text-center">
                  <a onClick={() => rejectAuthorizeBulk(authBulkPayment)}>
                    <X size={18} />
                  </a>
                </td>
              </tr>
            );
          })}
        </tbody>
      </Table>
    </div>
  );
}

export default AuthBulkPayment;
