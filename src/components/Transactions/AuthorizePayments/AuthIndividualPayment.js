import React, { useEffect } from "react";
import { Table } from "reactstrap";
import { Download, Check, X } from "react-feather";
import { useDispatch, useSelector } from "react-redux";
import {
  getAuthIndividualPayments,
  rejectAuthorizeIndividualPayment,
  getBalance,
  approveAuthIndividualPayment,
  getDownloadFile,
} from "../../../actions/createAllBulkPaymentAction";

function AuthIndividualPayment(props) {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getAuthIndividualPayments());
    dispatch(getBalance());
  }, []);

  const authIndividualPayments = useSelector(
    (state) => state.transactionReducer.authIndividualPayment.transactions
  );
  const balance = useSelector((state) => state.transactionReducer.balance);

  function rejectAuthorizeIndividual(authIndividualPayment) {
    const fileID = authIndividualPayment._id;
    alert("Are you sure you want to reject this payment?");
    dispatch(rejectAuthorizeIndividualPayment(fileID));
  }

  function submitApprove(authIndividualPayment) {
    const approveFileId = authIndividualPayment._id;
    alert("Are you sure you want to approve this payment?");
    dispatch(approveAuthIndividualPayment(approveFileId));
  }

  function downloadFile(authIndividualPayment) {
    const downloadFileId = authIndividualPayment._id;
    dispatch(getDownloadFile(downloadFileId));
  }

  return (
    <div>
      <h6>Total balance held with NOW Money:AED {balance}</h6>

      <Table
        responsive
        className="tableStyle"
        className="table-style-transaction"
      >
        <thead>
          <tr>
            <th>Emirates ID</th>
            <th>Type</th>
            <th>Employee name</th>
            <th>Amount</th>
            <th>Description</th>
            <th>Status</th>
            <th>Createted at</th>
            <th>Updated at</th>
            <th>Schduled For</th>
            <th> Download</th>
            <th>Approve</th>
            <th>Reject</th>
          </tr>
        </thead>
        <tbody>
          {authIndividualPayments?.map((authIndividualPayment, i) => {
            return (
              <tr key={i}>
                <td>
                  {authIndividualPayment.individualSalaryFileEid || "N/A"}
                </td>
                <td>{authIndividualPayment.fileType || "N/A"}</td>
                <td>{authIndividualPayment.userFileName || "N/A"}</td>
                <td>{authIndividualPayment.amount || "N/A"}</td>
                <td>{authIndividualPayment.description || "N/A"}</td>
                <td>{authIndividualPayment.status || "N/A"}</td>
                <td>{authIndividualPayment.uploadedAt || "N/A"}</td>
                <td>{authIndividualPayment.statusUpdatedAt || "N/A"}</td>
                <td>{authIndividualPayment.scheduledFor || "ASAP"}</td>
                <td className="table-text-center">
                  <a onClick={() => downloadFile(authIndividualPayment)}>
                    {" "}
                    <Download size={18} />
                  </a>
                </td>
                <td className="table-text-center">
                  <a onClick={() => submitApprove(authIndividualPayment)}>
                    <Check size={18} />
                  </a>
                </td>
                <td className="table-text-center">
                  <a
                    onClick={() =>
                      rejectAuthorizeIndividual(authIndividualPayment)
                    }
                  >
                    <X size={18} />
                  </a>
                </td>
              </tr>
            );
          })}
        </tbody>
      </Table>
    </div>
  );
}

export default AuthIndividualPayment;
