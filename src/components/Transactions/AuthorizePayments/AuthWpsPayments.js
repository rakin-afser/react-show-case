import React, { useEffect } from "react";
import { Table } from "reactstrap";
import { useDispatch, useSelector } from "react-redux";
import {
  getAuthWpsPayments,
  getBalance,
} from "../../../actions/createAllBulkPaymentAction";

function AuthWpsPayments(props) {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getAuthWpsPayments());
    dispatch(getBalance());
  }, []);

  const authWpsPayments = useSelector(
    (state) => state.transactionReducer.authWpsPayment.transactions
  );
  const balance = useSelector((state) => state.transactionReducer.balance);

  return (
    <div>
      <h6>Total balance held with NOW Money:AED {balance}</h6>

      <Table responsive className="table-style-transaction">
        <thead>
          <tr>
            <th>Payment ID</th>
            <th>File Name</th>
            <th>Amount</th>
            <th>Fees</th>
            <th>Status</th>
            <th>Createted at</th>
            <th>Updated at</th>
            <th> Download</th>
            <th>Approve</th>
            <th>Reject</th>
          </tr>
        </thead>
        <tbody></tbody>
      </Table>
    </div>
  );
}

export default AuthWpsPayments;
