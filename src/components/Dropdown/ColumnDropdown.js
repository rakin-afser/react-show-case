import React, {useState} from 'react';
import {
    Table,
    Button,
    ButtonGroup,
    Row,
    Col,
    Dropdown,
    UncontrolledDropdown,
    DropdownToggle,
    DropdownMenu,
    DropdownItem,
    Input,
    FormGroup
  } from "reactstrap";
function ColumnDropdown(props) {

    console.log(".........",props.setIbane)
    return (
        <div>
            <ButtonGroup>
          
          <Button>Display all</Button>
          <UncontrolledDropdown inNavbar className="pr-1">
                      <DropdownToggle >
                         Display
                       </DropdownToggle>
                      <DropdownMenu right>
                         <DropdownItem>
                           <Input type="checkbox" id="checkbox1" /> 
                           Employee name</DropdownItem>
                         <DropdownItem>                                                  
                         <Input type="checkbox" id="checkbox2" />                      
                         Emirates ID </DropdownItem>
                         <DropdownItem>
                         <Input checked={props.iban} onChange={()=> props.setIbane} type="checkbox" id="checkbox3" />
                           IBAN</DropdownItem>
                         <DropdownItem>
                         <Input type="checkbox" id="checkbox4" />
                           Reference ID</DropdownItem>
                         <DropdownItem>
                         <Input type="checkbox" id="checkbox5" />
                           Designation</DropdownItem>
                         <DropdownItem>
                         <Input type="checkbox" id="checkbox6" />
                           Department</DropdownItem>
                         <DropdownItem>
                         <Input type="checkbox" id="checkbox7" />
                           Account active since</DropdownItem>
                         <DropdownItem>
                         <Input type="checkbox" id="checkbox8" />
                           Account status</DropdownItem>
                         <DropdownItem>
                         <Input type="checkbox" id="checkbox9" />
                           Remove</DropdownItem>
                      </DropdownMenu>
                   </UncontrolledDropdown>

        </ButtonGroup>
        </div>
    );
}

export default ColumnDropdown;