import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Row,
  Col,
  Input,
  Form,
  FormGroup,
  Label,
} from "reactstrap";
import { Upload } from "react-feather";
import { addEmployee } from "../../actions/employeeActions";
const AddEmployee = (props) => {
  const [formData, setformData] = useState({
    name: "",
    id: "",
    mobile: "",
    department: "",
    refId: "",
    position: "",
    errors: {
      name: "",
      id: "",
      mobile: "",
      department: "",
      refId: "",
      position: ""
    },
  });
  const { className } = props;
  const { name, id, mobile, department, refId, position, errors } = formData;
  
  const dispatch = useDispatch();

  const handleChange = (e) => {
    console.log('-----------f--', { ...formData, [e.target.name]: e.target.value })
    setformData({ ...formData, [e.target.name]: e.target.value });
  };


  const handleSubmit = (e) => {
    e.preventDefault();
    dispatch(addEmployee(name, id, mobile, department, refId, position));
  };
  return (

    <Modal isOpen={props.showEmployeeForm} className={className} centered>
      <ModalHeader toggle={() => props.openModal(false)}>Employee Information</ModalHeader>
      {<ModalBody>
        <Form className="pt-2" onSubmit={handleSubmit}>
          <FormGroup>
            <Col md="12" className="pull-left">
              <Label for="inputName">Employee name</Label>
              <Input
                type="text"
                className="form-control"
                name="name"
                onChange={handleChange}
                id="inputName"
                placeholder=" name"
                required
              />
            </Col>
          </FormGroup>
          <FormGroup>
            <Col md="12" className="pull-left">
              <Label for="inputId">Employee Emirates id number</Label>
              <Input
                type="text"
                className="form-control"
                name="id"
                onChange={handleChange}
                id="inputId"
                placeholder="employee id"
                required
              />
            </Col>
          </FormGroup>
          <FormGroup>
            <Col md="12" className="pull-left">
              <Label for="inputMobile">Mobile number</Label>
              <Input
                type="number"
                onChange={handleChange}
                className="form-control"
                name="mobile"
                id="inputMobile"
                placeholder="mobile no"
                required
              />
            </Col>
          </FormGroup>

          <FormGroup>
            <Col md="12" className="pull-left">
              <Label for="inputPosition">Employee Designation</Label>
              <Input
                type="text"
                onChange={handleChange}
                className="form-control"
                name="position"
                placeholder="position"
                id="inputPosition"
                required
              />
            </Col>
          </FormGroup>
          <FormGroup>
            <Col md="12" className="pull-left">
              <Label for="inputDepartment">Employee Department</Label>
              <Input
                type="text"
                onChange={handleChange}
                className="form-control"
                name="department"
                placeholder="department"
                id="inputDepartment"
                required
              />
            </Col>
          </FormGroup>
          <FormGroup>
            <Col md="12" className="pull-left">
              <Label for="inputrefId">Refference Id</Label>
              <Input
                type="text"
                onChange={handleChange}
                className="form-control"
                name="refId"
                placeholder="refId"
                id="inputrefId"
                required
              />
            </Col>
          </FormGroup>
          <FormGroup>
            <Col md="12">
              <Button
                type="submit"
                color="primary"
                block
                className="btn-raised"
              >
                Submit
                </Button>
            </Col>
          </FormGroup>
        </Form>
      </ModalBody>}
    </Modal>

  );
};

export default AddEmployee;
