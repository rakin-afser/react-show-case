import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
    Button,
    Modal,
    ModalHeader,
    ModalBody,
    Col,
    Input,
    Form,
    FormGroup,
    Label,
} from "reactstrap";
import { Check } from "react-feather";
import { addUser, getRole, updateRole } from "../../actions/authActions";
const AddUser = (props) => {
    const dispatch = useDispatch();
    const [formDataUser, setFormDataUser] = useState({
        fullName: "",
        username: "",
        mobileNumber: "",
        email: "",
        idCardNumber: "",
        roleId: "",
        errors: {
            fullName: "",
            username: "",
            mobileNumber: "",
            email: "",
            idCardNumber: "",
            roleId: "",

        },
    });

    const { className } = props;
    const { fullName, username, mobileNumber, email, idCardNumber, roleId, errors, role, active } = formDataUser;

    useEffect(() => {
        dispatch(getRole())
    }, [dispatch])

    const addUserRole = useSelector((state) => state.appReducer.userRole.roles)
    const handleChange = (e) => {
        setFormDataUser({ ...formDataUser, [e.target.name]: e.target.value });

    };

    const userId = props.id


    const handleSubmit = (e) => {
        e.preventDefault();

        if (props.editable === true) {
            dispatch(updateRole(userId, roleId));
        } else {
            dispatch(addUser(fullName, username, mobileNumber, email, idCardNumber, roleId));
        }
        props.toggle(false)


    };
    return (
        <>
            <Modal isOpen={props.isOpen} className={className} centered >
                <ModalHeader toggle={() => props.toggle(false)} className="float-left">{props.editable === true ? "Edit User" : "Add User"}</ModalHeader>
                {<ModalBody>

                    <Form className="pt-2" onSubmit={handleSubmit} >
                        <FormGroup>
                            <Col md="12" className="pull-left">
                                <Label for="inputfullName">Full Name</Label>
                                <Input
                                    disabled={props.editable ? true : false}
                                    type="text"
                                    className="form-control"
                                    name="fullName"
                                    onChange={handleChange}
                                    id="inputName"
                                    placeholder="Full name"
                                    required
                                    defaultValue={props.fullName}
                                />
                            </Col>
                        </FormGroup>
                        <FormGroup>
                            <Col md="12" className="pull-left">
                                <Label for="inputusername">User Name</Label>
                                <Input
                                    disabled={props.editable ? true : false}
                                    type="text"
                                    className="form-control"
                                    onChange={handleChange}
                                    name="username"
                                    id="inputusername"
                                    placeholder="Username"
                                    required
                                    defaultValue={props.userName}
                                />
                            </Col>
                        </FormGroup>
                        <FormGroup>
                            <Col md="12" className="pull-left">
                                <Label for="inputmobileNumber">Mobile number</Label>
                                <Input
                                    disabled={props.editable ? true : false}
                                    type="number"
                                    className="form-control"
                                    onChange={handleChange}
                                    name="mobileNumber"
                                    id="inputmobileNumber"
                                    placeholder="+971XXXXXXXXX"
                                    required
                                    defaultValue={props.mobileNumber }
                                />
                            </Col>
                        </FormGroup>

                        <FormGroup>
                            <Col md="12" className="pull-left">
                                <Label for="inputEmail">Email</Label>
                                <Input
                                    disabled={props.editable ? true : false}
                                    type="text"
                                    className="form-control"
                                    onChange={handleChange}
                                    name="email"
                                    placeholder="Email"
                                    id="inputEmail"
                                    required
                                    defaultValue={props.email }
                                />
                            </Col>
                        </FormGroup>
                        <FormGroup>
                            <Col md="12" className="pull-left">
                                <Label for="inputidCardNumber">ID Card Number</Label>
                                <Input
                                    disabled={props.editable ? true : false}
                                    type="text"
                                    className="form-control"
                                    onChange={handleChange}
                                    name="idCardNumber"
                                    placeholder="784XXXXXXXXXXXX"
                                    id="inputidCardNumber"
                                    required
                                    defaultValue={props.idCardNumber }
                                />
                            </Col>
                        </FormGroup>
                        <FormGroup>
                            <Col md="12" className="pull-left">
                                <Label for="inputroleId">Role</Label>

                                <Input type="select"
                                    defaultValue={props.roleId}
                                    name="roleId" id="inputroleId"
                                    onChange={handleChange}>
                                    {addUserRole?.map((role, i) => {
                                        return (
                                            <option key={i} value={role._id}>{role.name}</option>
                                        )
                                    })}

                                </Input>
                            </Col>
                        </FormGroup>
                        {props.editable === true ? <FormGroup>
                            <Col md="12">
                                <Button
                                    type="submit"
                                    color="primary"
                                    block
                                    className="btn-raised"
                                >
                                    <Check />
                            Update
                        </Button>
                            </Col>
                        </FormGroup>
                            : <FormGroup>
                                <Col md="12">
                                    <Button
                                        type="submit"
                                        color="primary"
                                        block
                                        className="btn-raised"
                                    >
                                        <Check />
                            Save
                        </Button>
                                </Col>
                            </FormGroup>}
                    </Form>


                </ModalBody>}
            </Modal>

        </>

    );
};

export default AddUser;


