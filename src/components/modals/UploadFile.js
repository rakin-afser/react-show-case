import React, { useState, useCallback } from "react";
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from "reactstrap";
// import { useDropzone } from 'react-dropzone';
import UploadData from "rc-upload";
import { Upload } from "react-feather";
import { useSelector, useDispatch } from "react-redux";
import styled from "styled-components";
import { uploadEmployee } from "../../actions/employeeActions";

const getColor = (props) => {
  if (props.isDragAccept) {
    return "#00e676";
  }
  if (props.isDragReject) {
    return "#ff1744";
  }
  if (props.isDragActive) {
    return "#2196f3";
  }
  return "#eeeeee";
};

const Container = styled.div`
  flex: 1;
  display: flex;
  flex-direction: column;
  align-items: center;
  padding: 20px;
  border-width: 2px;
  border-radius: 2px;
  border-color: ${(props) => getColor(props)};
  border-style: dashed;
  background-color: #fafafa;
  color: #bdbdbd;
  outline: none;
  transition: border 0.24s ease-in-out;
`;
const UploadFile = (props) => {
  const [uploadableFile, setUploadableFile] = useState(null);
  const { className } = props;

  const dispatch = useDispatch();
  const uploadFiles = () => {
    // let formData = new FormData();
    console.log('------------', uploadableFile)
    const { name } = uploadableFile;
    const toBase64 = file => new Promise((resolve, reject) => {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => resolve(reader.result);
      reader.onerror = error => reject(error);
  });
  toBase64(uploadableFile).then(file => {
    console.log('fff', file)
    dispatch(uploadEmployee(file, false, name))
  }).catch(e => {
    console.log('e --- ', e)
  })
    
    // if (uploadableFile) {

    //   for (var i = 0; i < uploadableFile.length; i++) {
    //     let file = uploadableFile[i];
    //     console.log("------ file ", file);

    //   }


    // }
  };
  const handleFile = (file) => {
    console.log("file ----- ", file);
    setUploadableFile(file)
  };

  return (
    <div>
      <Modal
        isOpen={props.isOpen}
        className={className}
        centered
      >
        <ModalHeader toggle={()=> props.toggle(false)}>Add Employee</ModalHeader>
        <ModalBody>
          <div className="container">
            <Container>
              <UploadData onStart={handleFile}><p>Drag 'n' drop some files here, or click to select files</p></UploadData>
            </Container>
          </div>
        </ModalBody>
        <ModalFooter>
          <Button color="success" block onClick={uploadFiles}>
            <Upload />UploadFile
          </Button>{" "}
        </ModalFooter>
      </Modal>
    </div>
  );
};

export default UploadFile;
