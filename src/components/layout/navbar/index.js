import React, { useState } from "react";
import { Link } from "react-router-dom";
import {
   Collapse,
   Navbar,
   Nav,
   UncontrolledDropdown,
   DropdownToggle,
   DropdownMenu,
   DropdownItem,
} from "reactstrap";
import {
   Menu,
   MoreVertical,
   User,
   LogOut,
} from "react-feather";


import userImage from "../../../assets/img/portrait/small/avatar-s-1.png";
import UPloadFile from "../../modals/UploadFile"
import AddEmployee from "../../modals/AddEmployee"
function ThemeNavbar(props) {
   const [isOpen, setIsOpen] = useState(false);
   const [showEmployeeForm, setShowEmployeeForm] = useState(false);
   function handleClick(e) {
      props.toggleSidebarMenu("open");
   };

   function toggle() {
      setIsOpen(!isOpen)
   }
   return (
      <Navbar className="navbar navbar-expand-lg navbar-light bg-faded">
         <div className="container-fluid px-0">
            <div className="navbar-header">
               <Menu
                  size={14}
                  className="navbar-toggle d-lg-none float-left"
                  onClick={handleClick}
                  data-toggle="collapse"
               />
               {
                  props.matchProps.match.path === "/reviweCompanyInfo" ? <h5 className="content-header">Review Company Details</h5>
                     : props.matchProps.match.path === "/permissions" ? <h5 className="content-header">Permissions</h5>
                        : props.matchProps.match.path === "/uploadTransferProof" ? <h5 className="content-header">Load Corporate account</h5>
                           :props.matchProps.match.path === "/wpsStatus" ?<h5 className="content-header">WPS Payments</h5>
                           :props.matchProps.match.path === "/authorizeBulkPayment" ?<h5 className="content-header" >Authorize Bulk Payments</h5>
                           :props.matchProps.match.path === "/authorizeIndividualPayment" ?<h5 className="content-header">Authorize Individual Payments</h5>
                           :props.matchProps.match.path === "/authorizeWpsPayment" ?<h5 className="content-header">Authorize WPS Payments</h5>
                           :props.matchProps.match.path === "/validateBulkPayment" ?<h5 className="content-header">Validate Bulk Payment</h5>
                           :props.matchProps.match.path === "/validateIndividualPayment" ?<h5 className="content-header">Validate Individual Payment</h5>
                           :props.matchProps.match.path === "/validateWpsPayment" ?<h5 className="content-header">Validate WPS Payment</h5>
                           :props.matchProps.match.path === "/createBulkDisbursement" ?<h5 className="content-header">Create Bulk Payment</h5>
                           :props.matchProps.match.path === "/createIndividualDisbursement" ?<h5 className="content-header">Create Individual Payment</h5>
                           :props.matchProps.match.path === "/createWPSBulkDisbursement" ?<h5 className="content-header">Create WPS Payment</h5>
                        : <h5 className="content-header">Dashboard</h5>
               }
               <MoreVertical
                  className="mt-1 navbar-toggler black no-border float-right"
                  size={50}
                  onClick={toggle}
               />
            </div>

            <div className="navbar-container">
               <Collapse isOpen={isOpen} navbar>
                  <Nav className="ml-auto float-right" navbar>

                     {/* <UncontrolledDropdown nav inNavbar className="pr-1">
                        <DropdownToggle caret style={{ marginTop: "7px" }}>
                           Show All
                         </DropdownToggle>
                        <DropdownMenu>
                           <DropdownItem>Account active</DropdownItem>
                           <DropdownItem>Card generated</DropdownItem>
                           <DropdownItem>Registration successful</DropdownItem>
                           <DropdownItem>Registration complete</DropdownItem>
                           <DropdownItem>Registration failed</DropdownItem>
                           <DropdownItem>Registration started</DropdownItem>
                        </DropdownMenu>
                     </UncontrolledDropdown> */}

                     {/* <NavItem className="pr-1">
                        <div className="navbar-form mt-1 float-left" role="search">
                           <NavbarSearch />
                        </div>

                     </NavItem> */}
                     {/* <NavItem className="pr-1">
                        <Form className="navbar-form mt-1 float-left" role="search">
                           <NavbarSearch />
                        </Form>
                     </NavItem> */}
                     {/* <UncontrolledDropdown nav inNavbar className="pr-1">
                        <DropdownToggle nav caret>
                           <Plus />
                        </DropdownToggle>
                        <DropdownMenu right>
                           <DropdownItem header>Add employee</DropdownItem>
                           <DropdownItem><a onClick={toggle}> <Users size={18} />
                           Upload file</a>
                           </DropdownItem>
                           <DropdownItem>
                              <a onClick={() => {
                                 setShowEmployeeForm(!showEmployeeForm)
                              }}>
                                 <User size={18} />
                                 Enter details</a>
                           </DropdownItem>
                           <DropdownItem><UserPlus size={18} />
                           Update entire list</DropdownItem>
                        </DropdownMenu>
                     </UncontrolledDropdown>
                     <UncontrolledDropdown nav inNavbar className="pr-1">
                        <DropdownToggle nav caret>
                           <Download size={18} />
                        </DropdownToggle>
                        <DropdownMenu right>
                           <DropdownItem header>Export employees</DropdownItem>
                           <DropdownItem><Download size={18} />Download file</DropdownItem>
                           <DropdownItem header>Employee addition formate</DropdownItem>
                           <DropdownItem><a target="_blank" href="/employeeFile"><Download size={18} />Download</a></DropdownItem>
                        </DropdownMenu>
                     </UncontrolledDropdown> */}




                     {/* <UncontrolledDropdown nav inNavbar className="pr-1">
                        <DropdownToggle nav>
                           <a onClick={() => setAddUserModal(!isshowAddUsermodal)}> <Plus /></a>
                        </DropdownToggle>
                     </UncontrolledDropdown> */}


                     {/* <UncontrolledDropdown nav inNavbar className="pr-1">
                        <DropdownToggle nav>
                           <span className="notification-bell-blink" />
                           <Bell size={21} className="text-dark notification-danger animate-shake" />
                        </DropdownToggle>
                        <DropdownMenu right className="notification-dropdown">
                           <div className="p-2 text-center  border-bottom-grey border-bottom-lighten-2">
                              <h6 className="mb-0 text-bold-500">Notifications</h6>
                           </div>
                           <PerfectScrollbar className="noti-list bg-grey bg-lighten-5">
                              <Media className="px-3 pt-2 pb-2 media  border-bottom-grey border-bottom-lighten-3">
                                 <Media left top href="#">
                                    <Media
                                       object
                                       src={userImage2}
                                       alt="Generic placeholder image"
                                       className="rounded-circle width-35"
                                    />
                                 </Media>
                                 <Media body>
                                    <h6 className="mb-0 text-bold-500 font-small-3">
                                       Selina sent you mail
                                          <span className="text-bold-300 font-small-2 text-muted float-right">9:00 A.M</span>
                                    </h6>
                                    <span className="font-small-3 line-height-2">
                                       Cras sit amet nibh libero, in gravida nulla.
                                       </span>
                                 </Media>
                              </Media>
                              <Media className="px-3 pt-2 pb-2 media  border-bottom-grey border-bottom-lighten-3">
                                 <Media left middle href="#" className="mr-2">
                                    <span className="bg-success rounded-circle width-35 height-35 d-block">
                                       <Check size={30} className="p-1 white margin-left-3" />
                                    </span>
                                 </Media>
                                 <Media body>
                                    <h6 className="mb-1 text-bold-500 font-small-3">
                                       <span className="success">Report generated successfully!</span>
                                       <span className="text-bold-300 font-small-2 text-muted float-right">
                                          10:15 A.M
                                          </span>
                                    </h6>
                                    <span className="font-small-3 line-height-2">
                                       Consectetur adipisicing elit sed do eiusmod.
                                       </span>
                                 </Media>
                              </Media>
                              <Media className="px-3 pt-2 pb-2 media  border-bottom-grey border-bottom-lighten-3">
                                 <Media left middle href="#" className="mr-2">
                                    <span className="bg-warning rounded-circle width-35 height-35 d-block">
                                       <AlertTriangle size={30} className="p-1 white margin-left-3" />
                                    </span>
                                 </Media>
                                 <Media body>
                                    <h6 className="mb-1 text-bold-500 font-small-3">
                                       <span className="warning">Warning notificatoin</span>
                                       <span className="text-bold-300 font-small-2 text-muted float-right">
                                          11:00 A.M
                                          </span>
                                    </h6>
                                    <p className="font-small-3 line-height-2">
                                       Lorem ipsum dolor sit amet consectetur adipisicing elit sed do eiusmod tempor.
                                       </p>
                                 </Media>
                              </Media>
                              <Media className="px-3 pt-2 pb-2 media  border-bottom-grey border-bottom-lighten-3">
                                 <Media left top href="#">
                                    <Media
                                       object
                                       src={userImage3}
                                       alt="Generic placeholder image"
                                       className="rounded-circle width-35"
                                    />
                                 </Media>
                                 <Media body>
                                    <h6 className="mb-0 text-bold-500 font-small-3">
                                       John started task
                                          <span className="text-bold-300 font-small-2 text-muted float-right">5:00 P.M</span>
                                    </h6>
                                    <span className="font-small-3 line-height-2">
                                       Sit amet consectetur adipisicing elit sed.
                                       </span>
                                 </Media>
                              </Media>
                              <Media className="px-3 pt-2 pb-2 media  border-bottom-grey border-bottom-lighten-3">
                                 <Media left middle href="#" className="mr-2">
                                    <span className="bg-danger rounded-circle width-35 height-35 d-block">
                                       <X size={30} className="p-1 white margin-left-3" />
                                    </span>
                                 </Media>
                                 <Media body>
                                    <h6 className="mb-1 text-bold-500 font-small-3">
                                       <span className="danger">Error notificarion</span>
                                       <span className="text-bold-300 font-small-2 text-muted float-right">
                                          12:15 P.M
                                          </span>
                                    </h6>
                                    <span className="font-small-3 line-height-2">
                                       Consectetur adipisicing elit sed do eiusmod.
                                       </span>
                                 </Media>
                              </Media>
                              <Media className="px-3 pt-2 pb-2 media  border-bottom-grey border-bottom-lighten-3">
                                 <Media left top href="#">
                                    <Media
                                       object
                                       src={userImage4}
                                       alt="Generic placeholder image"
                                       className="rounded-circle width-35"
                                    />
                                 </Media>
                                 <Media body>
                                    <h6 className="mb-0 text-bold-500 font-small-3">
                                       Lisa started task
                                          <span className="text-bold-300 font-small-2 text-muted float-right">6:00 P.M</span>
                                    </h6>
                                    <span className="font-small-3 line-height-2">
                                       Sit amet consectetur adipisicing elit sed.
                                       </span>
                                 </Media>
                              </Media>
                           </PerfectScrollbar>
                           <div className="p-1 text-center border-top-grey border-top-lighten-2">
                              <Link to="/">View All</Link>
                           </div>
                        </DropdownMenu>
                     </UncontrolledDropdown> */}

                     <UncontrolledDropdown nav inNavbar className="pr-1">
                        <DropdownToggle nav>
                           <img src={userImage} alt="logged-in-user" className="rounded-circle width-35" />
                        </DropdownToggle>
                        <DropdownMenu right>
                           {/* <DropdownItem>
                              <span className="font-small-3">
                                 John Doe <span className="text-muted">(Guest)</span>
                              </span>
                           </DropdownItem>
                           <DropdownItem divider /> */}

                           <Link to="/user-profile" className="p-0">
                              <DropdownItem>
                                 <User size={16} className="mr-1" /> My Profile
                                 </DropdownItem>
                           </Link>
                           {/* <Link to="/email" className="p-0">
                              <DropdownItem>
                                 <Inbox size={16} className="mr-1" /> Email
                                 </DropdownItem>
                           </Link>
                           <Link to="/contacts" className="p-0">
                              <DropdownItem>
                                 <Phone size={16} className="mr-1" /> Contacts
                                 </DropdownItem>
                           </Link>
                           <Link to="/calendar" className="p-0">
                              <DropdownItem>
                                 <Calendar size={16} className="mr-1" /> Calendar
                                 </DropdownItem>
                           </Link>
                           <DropdownItem divider />
                           <Link to="/pages/lockscreen" className="p-0">
                              <DropdownItem>
                                 <Lock size={16} className="mr-1" /> Lock Screen
                                 </DropdownItem>
                           </Link> */}
                           <Link to="/pages/login" className="p-0">
                              <DropdownItem>
                                 <LogOut size={16} className="mr-1" /> Logout
                                 </DropdownItem>
                           </Link>
                        </DropdownMenu>
                     </UncontrolledDropdown>
                  </Nav>
               </Collapse>
            </div>
         </div>
         {
            isOpen ? <UPloadFile toggle={toggle} isOpen={isOpen} /> : null
         }
         {
            showEmployeeForm ? <AddEmployee openModal={setShowEmployeeForm} showEmployeeForm={showEmployeeForm} /> : null
         }
      </Navbar>
   );

}

export default ThemeNavbar;
