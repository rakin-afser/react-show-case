import React, { useState } from "react";
import { Link } from "react-router-dom";
import {
   Collapse,
   Navbar,
   Nav,
   UncontrolledDropdown,
   DropdownToggle,
   DropdownMenu,
   DropdownItem,
} from "reactstrap";
import {
   Menu,
   MoreVertical,
   User,
   LogOut,
   Plus,
} from "react-feather";
import userImage from "../../../assets/img/portrait/small/avatar-s-1.png";
import AddUser from "../../modals/AddUser"
function ThemeNavbar(props) {
   const [isOpen, setIsOpen] = useState(false);
   const [isshowAddUsermodal, setAddUserModal] = useState(false)
   function handleClick(e) {
      props.toggleSidebarMenu("open");
   };

   function toggle() {
      setIsOpen(!isOpen)
   }
   return (
      <Navbar className="navbar navbar-expand-lg navbar-light bg-faded">
         <div className="container-fluid px-0">
            <div className="navbar-header">
               <Menu
                  size={14}
                  className="navbar-toggle d-lg-none float-left"
                  onClick={handleClick}
                  data-toggle="collapse"
               />
                  <h5 className="content-header">Users</h5>
               <MoreVertical
                  className="mt-1 navbar-toggler black no-border float-right"
                  size={50}
                  onClick={toggle}
               />
            </div>
            <div className="navbar-container">
               <Collapse isOpen={isOpen} navbar>
                  <Nav className="ml-auto float-right" navbar>
                     <UncontrolledDropdown nav inNavbar className="pr-1">
                        <DropdownToggle nav>
                           <a onClick={() => setAddUserModal(!isshowAddUsermodal)}> <Plus /></a>
                        </DropdownToggle>
                     </UncontrolledDropdown>

                     <UncontrolledDropdown nav inNavbar className="pr-1">
                        <DropdownToggle nav>
                           <img src={userImage} alt="logged-in-user" className="rounded-circle width-35" />
                        </DropdownToggle>
                        <DropdownMenu right>
                         

                           <Link to="/user-profile" className="p-0">
                              <DropdownItem>
                                 <User size={16} className="mr-1" /> My Profile
                                 </DropdownItem>
                           </Link>
                        
                           <Link to="/pages/login" className="p-0">
                              <DropdownItem>
                                 <LogOut size={16} className="mr-1" /> Logout
                                 </DropdownItem>
                           </Link>
                        </DropdownMenu>
                     </UncontrolledDropdown>
                  </Nav>
               </Collapse>
            </div>
         </div>
         {isshowAddUsermodal ? <AddUser isOpen={isshowAddUsermodal} toggle={setAddUserModal} /> : null}
      </Navbar>
   );

}

export default ThemeNavbar;
