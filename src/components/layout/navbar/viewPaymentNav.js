import React, { useState } from "react";
import { Link } from "react-router-dom";
import {
    Collapse,
    Navbar,
    Nav,
    NavItem,
    UncontrolledDropdown,
    DropdownToggle,
    DropdownMenu,
    DropdownItem,
} from "reactstrap";
import {
    Menu,
    MoreVertical,
    User,
    LogOut,
    Download
} from "react-feather";
import userImage from "../../../assets/img/portrait/small/avatar-s-1.png";
import Search from "../../common/Search";

function ViewPaymentNav(props) {
    const [isOpen, setIsOpen] = useState(false);

    function handleClick(e) {
        props.toggleSidebarMenu("open");
    };

    function toggle() {
        setIsOpen(!isOpen)
    }
    return (
        <div>
            <Navbar className="navbar navbar-expand-lg navbar-light bg-faded">
                <div className="container-fluid px-0">
                    <div className="navbar-header">
                        <Menu
                            size={14}
                            className="navbar-toggle d-lg-none float-left"
                            onClick={handleClick}
                            data-toggle="collapse"
                        />
                        <h5 className="content-header">Payments</h5>

                        <MoreVertical
                            className="mt-1 navbar-toggler black no-border float-right"
                            size={50}
                            onClick={toggle}
                        />
                    </div>

                    <div className="navbar-container">
                        <Collapse isOpen={isOpen} navbar>
                            <Nav className="ml-auto float-right" navbar>

                                <NavItem className="pr-1">
                                    <div className="navbar-form mt-1 float-left" role="search">
                                        <Search />
                                    </div>

                                </NavItem>
                                {/* <NavItem className="pr-1">
                        <Form className="navbar-form mt-1 float-left" role="search">
                           <NavbarSearch />
                        </Form>
                     </NavItem> */}

                                <UncontrolledDropdown nav inNavbar className="pr-1">
                                    <DropdownToggle nav caret>
                                        <Download size={18} />
                                    </DropdownToggle>
                                    <DropdownMenu right>
                                        <DropdownItem header>Export Transactions</DropdownItem>
                                        <DropdownItem><Download size={18} />Download file</DropdownItem>

                                    </DropdownMenu>
                                </UncontrolledDropdown>

                                <UncontrolledDropdown nav inNavbar className="pr-1">
                                    <DropdownToggle nav>
                                        <img src={userImage} alt="logged-in-user" className="rounded-circle width-35" />
                                    </DropdownToggle>
                                    <DropdownMenu right>

                                        <Link to="/user-profile" className="p-0">
                                            <DropdownItem>
                                                <User size={16} className="mr-1" /> My Profile
                                 </DropdownItem>
                                        </Link>
                                        <Link to="/pages/login" className="p-0">
                                            <DropdownItem>
                                                <LogOut size={16} className="mr-1" /> Logout
                                 </DropdownItem>
                                        </Link>
                                    </DropdownMenu>
                                </UncontrolledDropdown>
                            </Nav>
                        </Collapse>
                    </div>
                </div>

            </Navbar>
        </div>
    );
}

export default ViewPaymentNav;