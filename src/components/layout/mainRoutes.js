// import external modules
import React from "react";
import { Route } from "react-router-dom";
// import internal(own) modules
import DashboardLayout from "./DashboardLayout";

const MainLayoutRoute = ({ render, ...rest }) => {
   return (
      <Route
         {...rest}
         render={matchProps => (
            <DashboardLayout matchProps={matchProps}>{render(matchProps)}</DashboardLayout>
         )}
      />
   );
};

export default MainLayoutRoute;
