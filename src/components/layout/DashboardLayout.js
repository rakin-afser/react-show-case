import React, { useEffect, useState } from 'react';
import classnames from "classnames";
// import { FoldedContentConsumer, FoldedContentProvider } from '../../utility/toogleContentContext.js';
import templateConfig from '../../utility/templateConfig.js'
import Sidebar from "./sidebar/index.js";
import Navbar from './navbar/index'
import UserNavbar from './navbar/userNavbar'
import EmployeeNavbar from './navbar/employeeNavbar'
import ViewPaymentNav from "./navbar/viewPaymentNav";
import { FoldedContentConsumer, FoldedContentProvider } from '../../utility/toogleContentContext.js';
function DashboardLayout(props) {
   useEffect(() => {
      if (window !== "undefined") {
         window.addEventListener("resize", updateWidth, false);
      }
      // Alternative to componentwillunmount
      return function cleanup() {
         if (window !== "undefined") {
            window.removeEventListener("resize", updateWidth, false);
         }
      }
   })
   const [width, setWidth] = useState(window.innerWidth);
   const [sidebarState, setSidebarState] = useState('close');
   const [sidebarSize, setSidebarSize] = useState('');
   const [layout, setLayout] = useState('')
   const updateWidth = () => {
      setWidth(window.innerWidth)
   }
   const handleSidebarSize = (sidebarSize) => {
      setSidebarSize(sidebarSize)
   }
   const handleLayout = (layout) => {
      setLayout(layout)
   }
   const toggleSidebarMenu = (sidebarState) => {
      setSidebarState(sidebarState)
   }
   return (
      <FoldedContentProvider>
         <FoldedContentConsumer>
            {context => (

               <div
                  className={classnames("wrapper ", {
                     "menu-collapsed": context.foldedContent || width < 991,
                     "main-layout": !context.foldedContent,
                     [`${templateConfig.sidebar.size}`]: (sidebarSize === ''),
                     [`${sidebarSize}`]: (sidebarSize !== ''),
                     //    "layout-dark": (this.state.layout === 'layout-dark'),
                     //    " layout-dark": (this.state.layout === '' && templateConfig.layoutDark === true)
                     [`${templateConfig.layoutColor}`]: (layout === ''),
                     [`${layout}`]: (layout !== '')
                  })}
               >

                  <Sidebar
                     toggleSidebarMenu={toggleSidebarMenu}
                     sidebarState={sidebarState}
                     handleSidebarSize={handleSidebarSize}
                     handleLayout={handleLayout}
                  />
                  {
                     props.matchProps.match.path === "/employees" ? <EmployeeNavbar
                        toggleSidebarMenu={toggleSidebarMenu}
                        sidebarState={sidebarState}

                     />
                        : props.matchProps.match.path === "/users" ? <UserNavbar
                           toggleSidebarMenu={toggleSidebarMenu}
                           sidebarState={sidebarState}
                        />
                           // : props.matchProps.match.path === "/createBulkDisbursement" ? <CreateBulkNavbar
                           //    toggleSidebarMenu={toggleSidebarMenu}
                           //    sidebarState={sidebarState}
                           // />
                           //    : props.matchProps.match.path === "/createWPSBulkDisbursement" ? <CreateWpsNavbar
                           //       toggleSidebarMenu={toggleSidebarMenu}
                           //       sidebarState={sidebarState}
                           //    />
                                 : props.matchProps.match.path === "/viewPayment" ? <ViewPaymentNav
                                    toggleSidebarMenu={toggleSidebarMenu}
                                    sidebarState={sidebarState}
                                 />
                                    : <Navbar
                                       toggleSidebarMenu={toggleSidebarMenu}
                                       sidebarState={sidebarState}
                                       matchProps={props.matchProps}
                                    />}
                  <main>{props.children}</main>
                  {/**<Footer /> */}
               </div>
            )}
         </FoldedContentConsumer>
      </FoldedContentProvider>
   )
}

export default DashboardLayout;