import React, { Component } from "react";
import { NavLink } from "react-router-dom";
import { ToggleLeft, ToggleRight, X } from "react-feather";
// import internal(own) modules
import {FoldedContentConsumer} from "../../../utility/toogleContentContext"
// import Logo from "../../../../assets/img/logo.png";
// import LogoDark from "../../../../assets/img/logo-dark.png";
import Logo from "../../../assets/img/now.png";
import LogoDark from "../../../assets/img/now.png";
import templateConfig from "../../../utility/templateConfig";

function SidebarHeader(props) {

   function handleClick() {
     props.toggleSidebarMenu("close");
   };

   
      return (
         <FoldedContentConsumer>
            {context => (
               <div className="sidebar-header">
                  <div className="logo clearfix">
                     <NavLink to="/" className="logo-text float-left">
                   
                        <div className="logo-img">
                           {templateConfig.sidebar.backgroundColor === "white" ? (
                              props.sidebarBgColor === "" || props.sidebarBgColor === "white" ? (
                                 <img src={LogoDark} alt="logo" />
                              ) : (
                                 <img src={Logo} alt="logo" />
                              )
                           ) : props.sidebarBgColor === "white" ? (
                              <img src={LogoDark} alt="logo" />
                           ) : (
                              <img src={Logo} alt="logo" />
                           )}                           
                        </div>
                        <span className="font-medium-2">Test User</span>
                     </NavLink>

                     <span className="nav-toggle d-none d-sm-none d-md-none d-lg-block">
                        {context.foldedContent ? (
                           <ToggleLeft onClick={context.makeNormalContent} className="toggle-icon" size={16} />
                        ) : (
                           <ToggleRight onClick={context.makeFullContent} className="toggle-icon" size={16} />
                        )}
                     </span>
                     <span href="" className="nav-close d-block d-md-block d-lg-none d-xl-none" id="sidebarClose">
                        <X onClick={handleClick} size={20} />
                     </span>
                  </div>
               </div>
            )}
         </FoldedContentConsumer>
      );
   }


export default SidebarHeader;