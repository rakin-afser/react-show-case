// import external modules
import React, { PureComponent, Children, cloneElement, Fragment, useState } from "react";
import { Badge } from "reactstrap";
// import internal(own) modules
import "../../../assets/scss/components/sidebar/sidemenuHelper.scss";

function SideMenuHelper(props) {
  const [selectedMenuItem, setSelectedMenuItem] = useState(-1);
  //  state = {
  //     selectedMenuItem: -1
  //  };

   // Toggle menu with dropdown
   function toggle(itemIndex) {
      // If menu is open
      if (selectedMenuItem === itemIndex) {
         setSelectedMenuItem(-1)
        //  this.setState({
        //     selectedMenuItem: -1
        //  });
      } else {
        setSelectedMenuItem(itemIndex)
         // If menu is close
        //  this.setState({ selectedMenuItem: itemIndex });
      }
   };

   // Close other dropdown on single menu item click
   function closeOther (singleItemIndex) {
     setSelectedMenuItem(singleItemIndex)
      // this.setState(prevState => {
      //    return {
      //       selectedMenuItem: singleItemIndex
      //    };
      // });
   };

   function handleClick(e ) {
      props.toggleSidebarMenu("close");
   };

  //  static MenuSingleItem = props => ( 
  //     <li
  //        className="nav-item"
  //        onClick={() => {
  //           props.closeOther(props.index);
  //           props.handleClick();
  //        }}
  //     >
  //        {props.children}
  //        {props.badgeText ? (
  //           <Badge className="menu-item-badge menu-single-item-badge" color={props.badgeColor} pill>
  //              {props.badgeText}
  //           </Badge>
  //        ) : (
  //           ""
  //        )}
  //     </li>
  //  );
  function MenuSingleItem (props) {
    <li
       className="nav-item"
       onClick={() => {
          props.closeOther(props.index);
          props.handleClick();
       }}
    >
       {props.children}
       {props.badgeText ? (
          <Badge className="menu-item-badge menu-single-item-badge" color={props.badgeColor} pill>
             {props.badgeText}
          </Badge>
       ) : (
          ""
       )}
    </li>
       }

  //  static MenuMultiItems = props => (
    function MenuMultiItems(props) {
      <Fragment>
         <li className={`has-sub nav-item  ${props.selected === true && props.collapsedSidebar === false ? `open` : ""}`}>
             {/* eslint-disable-next-line */}
            <a
               onClick={() => {
                  props.toggle(props.index);
               }}
            >
               <i className="menu-icon">{props.Icon ? props.Icon : null}</i>
               <span className="menu-item-text d-inline"> {props.name}</span>
               {props.badgeText ? (
                  <Badge className="menu-item-badge menu-multiple-item-badge" color={props.badgeColor} pill>
                     {props.badgeText}
                  </Badge>
               ) : (
                  ""
               )}

               <span className="item-arrow">{props.ArrowRight}</span>
            </a>
            
            <ul>
               {Children.map(props.children, (child, index) => {
                  if (child.props.children.type === undefined) {
                     return <li>{child}</li>;
                  } else {
                     return (
                        <li
                           onClick={() => {
                              props.handleClick();
                           }}
                        >
                           {child}
                        </li>
                     );
                  }
               })}
            </ul>
         </li>
      </Fragment>
              }

   
      const Nodes = Children.map(props.children, (child, index) => {
         if (Boolean(child.type === MenuSingleItem)) {
            return cloneElement(child, {
               closeOther: closeOther,
               handleClick: handleClick,
               index: index,
               selected: index === selectedMenuItem ? true : false,
               ...child.props
            });
         }
         if (Boolean(child.type === MenuMultiItems)) {
            return cloneElement(child, {
               toggle: toggle,
               handleClick: handleClick,
               index: index,
               selected: index === selectedMenuItem ? true : false,
               collapsedSidebar: child.props.collapsedSidebar !== undefined ? child.props.collapsedSidebar : false,
               ...child.props
            });
         }
      });
      return (
         <div className="nav-container">
            <ul id="main-menu-navigation" className="navigation navigation-main">
               {Nodes}
            </ul>
         </div>
      );
   
}

export default SideMenuHelper;
