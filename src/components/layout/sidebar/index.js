import React, { Fragment, useEffect, useState } from "react";
import classnames from "classnames";
import { useSelector, useDispatch } from 'react-redux'
import PerfectScrollbar from "react-perfect-scrollbar";
/** Styling */
import "../../../assets/scss/components/sidebar/sidebar.scss";
import { FoldedContentConsumer } from "../../../utility/toogleContentContext";
import SidebarHeader from "./sidebarHeader";
import SideMenuContent from "./sidebarMenu";
import templateConfig from "../../../utility/templateConfig";
import Customizer from "../../customizer/customizer.js"
function Sidebar(props) {

  const { color, img, imgurl, size, collapsed } = useSelector(state => ({
    color: state.customizer.sidebarBgColor,
    img: state.customizer.sidebarImage,
    imgurl: state.customizer.sidebarImageUrl,
    size: state.customizer.sidebarSize,
    collapsed: state.customizer.sidebarCollapsed

  }));



  const [collapsedSidebar, setCollapsedSidebar] = useState(
    templateConfig.sidebar.collapsed
  );
  const [width, setWidth] = useState(window.innerWidth);
  useEffect(() => {
    if (window !== "undefined") {
      window.addEventListener("resize", updateWidth, false);
    }
    return function cleanup() {
      if (window !== "undefined") {
        window.removeEventListener("resize", updateWidth, false);
      }
    };
  });
  function updateWidth() {

    setWidth(window.innerWidth);
  }

  function handleCollapsedSidebar(collapsedSidebar) {

    setCollapsedSidebar(collapsedSidebar);
  }


  function handleMouseEnter(e) {

    setCollapsedSidebar(false);
  }

  function handleMouseLeave(e) {

    setCollapsedSidebar(true);
  }

  return (
    <Fragment>
      <FoldedContentConsumer>
        {(context) => (
          <div
            data-active-color="white"
            data-background-color={
              color === ""
                ? templateConfig.sidebar.backgroundColor
                : color
            }
            className={classnames(
              "app-sidebar",
              {
                "": !collapsedSidebar,
                collapsed: collapsedSidebar,
              },
              {
                "hide-sidebar": width < 991 && props.sidebarState === "close",
                "": props.sidebarState === "open",
              }
            )}
            onMouseEnter={context.foldedContent ? handleMouseEnter : null}
            onMouseLeave={context.foldedContent ? handleMouseLeave : null}
          >
            <SidebarHeader
              toggleSidebarMenu={props.toggleSidebarMenu}
              sidebarBgColor={color}
            />


            <PerfectScrollbar className="sidebar-content">

              <SideMenuContent
                collapsedSidebar={collapsedSidebar}
                toggleSidebarMenu={props.toggleSidebarMenu}
              />

            </PerfectScrollbar>

            {/* {this.props.img === '' ? ( */}
            {templateConfig.sidebar.backgroundImage ? (
              this.props.imgurl === "" ? (
                <div
                  className="sidebar-background"
                  style={{
                    backgroundImage:
                      "url('" +
                      templateConfig.sidebar.backgroundImageURL +
                      "')",
                  }}
                ></div>
              ) : (
                  <div
                    className="sidebar-background"
                    style={{
                      backgroundImage: "url('" + imgurl + "')",
                    }}
                  ></div>
                )
            ) : props.imgurl === "" ? (
              <div className="sidebar-background"></div>
            ) : (
                  <div
                    className="sidebar-background"
                    style={{ backgroundImage: "url('" + imgurl + "')" }}
                  ></div>
                )}
          </div>
        )}
      </FoldedContentConsumer>


      {/* <Customizer
        sidebarBgColor={props.sidebarBgColor}
        sidebarImageUrl={props.sidebarImageUrl}
        sidebarCollapsed={props.sidebarCollapsed}
        handleSidebarSize={props.handleSidebarSize}
        handleLayout={props.handleLayout}
        handleCollapsedSidebar={handleCollapsedSidebar}
      /> */}
    </Fragment>
  );
}

export default Sidebar;
