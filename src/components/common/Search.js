import React, { useState } from "react";
import { Input } from "reactstrap";
import { Search } from "react-feather";
import { useDispatch } from 'react-redux';
import { getSearch } from '../../actions/employeeActions';


function NavbarSearch() {
   const [searchText, setSearchText] = useState('');

   const dispatch = useDispatch();
   function handleChange(e) {

      setSearchText(e.target.value)

   };

   function handleEnter(e) {
      if (e.key === 'Enter') {
         dispatch(getSearch(searchText))
      }
   }

   return (
      <div className="position-relative has-icon-right">
         <Input
            id="search-term"
            type="text"
            onKeyPress={handleEnter}
            className="form-control round"
            placeholder="Try quick search"
            onChange={handleChange}
            value={searchText}
         />
         {/* <div className="control-position">
            <Search size={16} className="mb-0" />
         </div> */}

      </div>


   );

}

export default NavbarSearch;
