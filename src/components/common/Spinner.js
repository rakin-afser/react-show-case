import React, { useState } from "react";
import { ClipLoader } from "react-spinners";
import { themeColor } from "../constants/Color";

function SpinnerComponent() {
  const [loading, setLoading] = useState(true);

  return (
    <div className="sweet-loading">
      <ClipLoader
        className="clip-loader"
        sizeUnit={"px"}
        size={60}
        color={themeColor.PRIMARY}
        loading={loading}
      />
    </div>
  );
}

export default SpinnerComponent;
