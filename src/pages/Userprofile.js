
import React, { useEffect } from 'react';
import { Link } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { Form, Label, Input, Container, Row, Col, Card } from 'reactstrap';
import photo14 from "../../assets/img/portrait/small/avatar-s-1.png";
import avatarm8 from "../../assets/img/portrait/small/avatar-s-4.png";
import { getUser } from "../../actions/authActions";

function UserProfile(props) {
    const dispatch = useDispatch();


    useEffect(() => {

        dispatch(getUser())
    }, []);
    let user = useSelector((state) => state.authReducer)
    //     let users = JSON.parse(user)
    return (
        <Container >
            <div style={{ marginTop: "40px" }}>

                <Row>
                    <Col xs="12" id="user-profile">
                        <Card className="profile-with-cover">
                            {/* <div
                              className="card-img-top img-fluid bg-cover height-300"
                               style={{ background: `url("${photo14}") 50%` }}
                             /> */}
                            <Row className="media profil-cover-details">
                                <Col xs="4"> </Col>
                                <Col xs="3">
                                    <div className="align-self-center halfway-fab text-center">
                                        <Link to="/user-profile" className="profile-image">
                                            <img
                                                src={avatarm8}
                                                className="rounded-circle img-border gradient-summer width-100"
                                                alt="Card avatar"
                                            />
                                        </Link>
                                    </div>
                                </Col>
                                <Col xs="5"></Col>
                            </Row>
                            <div className="profile-section">
                                <Row>
                                    <Col lg="4" md="4">  </Col>
                                  <Col lg="3" md="3" className="text-center">
                                        <span className="font-medium-2 text-uppercase">Jose Diaz</span>
                                        <p className="grey font-small-2">Ninja Developer</p>
                                    </Col>
                                    <Col lg="5" md="5"></Col>
                                </Row>
                            </div>
                        </Card>
                    </Col>
                </Row>
                <Row>
                    <Col xs="2"></Col>
                    <Col xs="10">
                        <Form>
                            <Row>
                                {/* <Col xs="2"></Col> */}
                               
                                <Col xs="12"><h6>Account Owner Information</h6></Col>
                                {/* <Col xs="4"></Col> */}
                            </Row><br />
                            <Row>

                                <Col xs="2"> <Label for="name">Name</Label></Col>
                                <Col xs="1"></Col>
                                <Col xs="6">MD.Rakin Afser</Col>
                               {/* <Col xs="6"><Input type="text" name="name" id="name" /></Col> */}
                                <Col xs="3"></Col>
                            </Row><br />
                            <Row>
                                <Col xs="2"> <Label for="company">Company</Label></Col>
                                <Col xs="1"></Col>
                                <Col xs="6">Rakin Afser dev</Col>
                                {/* <Col xs="6"><Input type="text" name="company" id="company" /></Col> */}
                                <Col xs="3"></Col>
                            </Row><br />
                            <Row>
                                <Col xs="2"> <Label for="role">Role</Label></Col>
                                <Col xs="1"></Col>
                                <Col xs="6">ADMIN</Col>
                                {/* <Col xs="6"><Input type="text" name="role" id="role" /></Col> */}
                                <Col xs="3"></Col>
                            </Row><br />
                            <Row>
                                <Col xs="2"> <Label for="email">Email</Label></Col>
                                <Col xs="1"></Col>
                                <Col xs="6">test.rakin@gmail.com</Col>
                                {/* <Col xs="6"><Input type="email" name="email" id="email" /></Col> */}
                                <Col xs="3"></Col>
                           </Row><br />
                            <Row>
                                <Col xs="2"> <Label for="mobileNo">Mobile No</Label></Col>
                                <Col xs="1"></Col>
                                <Col xs="6">01712122210</Col>
                                {/* <Col xs="6"><Input type="number" name="mobileNo" id="mobileNo" /></Col> */}
                                <Col xs="3"></Col>
                            </Row><br />
                            <Row>
                                <Col xs="3"> <Label for="mobileNo">Reset Password</Label></Col>
                                <Col xs="1"></Col>
                                <Col xs="6"><a href="#">Click here to reset your password</a></Col>
                                <Col xs="2"></Col>
                            </Row><br />
                        </Form>
                    </Col>
                    <Col xs="1"></Col>
                </Row>
                <h6>E-mail notification settings</h6>
            </div>
        </Container>


    );
}

export default UserProfile;
