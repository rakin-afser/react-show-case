import React, { useState, useEffect } from "react";
import {
  Table,
  Button,
  ButtonGroup,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  Input,

} from "reactstrap";
import { Edit, Trash2 } from 'react-feather';
import { useSelector, useDispatch } from 'react-redux'
import { getEmployeesData } from '../../actions/employeeActions';
function Employee(props) {

  const [iban, setIban] = useState(true);
  const [employeeName, setEmployeeName] = useState(true);
  const [idCardNumber, setIdCardNumber] = useState(true);
  const [refId, setRefId] = useState(true);
  const [position, setPosition] = useState(true);
  const [department, setDepartment] = useState(true);
  const [createdAt, setCreatedAt] = useState(true);
  const [status, setStatus] = useState(true);
  const [remove, setRemove] = useState(true);

  const data = useSelector((state) => state.appReducer.employeesData)

  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getEmployeesData());
  }, [dispatch]);

  function displayAll() {
    if (employeeName === false) {
      setEmployeeName(!employeeName)
    }
    if (idCardNumber === false) {
      setIdCardNumber(!idCardNumber)
    }
    if (iban === false) {
      setIban(!iban)
    }
    if (refId === false) {
      setRefId(!refId)
    }
    if (position === false) {
      setPosition(!position)
    }
    if (department === false) {
      setCreatedAt(!department)
    }
    if (createdAt === false) {
      setDepartment(!createdAt)
    }
    if (status === false) {
      setStatus(!status)
    }
    if (remove === false) {
      setRemove(!remove)
    }
  }

  function renderColumn({ label, onChange, selected }) {
    return (
      <DropdownItem>
        <Input type="checkbox" id="checkbox1" checked={selected} onChange={() => onChange(!selected)} />
        <span className="text-gray">{label}</span>
      </DropdownItem>
    )
  }
  return (
    <>
      <div className="ml-auto float-right">
        <ButtonGroup>
          <Button onClick={displayAll}>Display all</Button>
          <UncontrolledDropdown inNavbar className="pr-1">
            <DropdownToggle >
              Display
                </DropdownToggle>
            <DropdownMenu right>
              {renderColumn({ label: "Employee Name", onChange: setEmployeeName, selected: employeeName })}
              {renderColumn({ label: " Emirates ID", onChange: setIdCardNumber, selected: idCardNumber })}
              {renderColumn({ label: " IBAN", onChange: setIban, selected: iban })}
              {renderColumn({ label: " Reference ID", onChange: setRefId, selected: refId })}
              {renderColumn({ label: " Designation", onChange: setPosition, selected: position })}
              {renderColumn({ label: " Department", onChange: setDepartment, selected: department })}
              {renderColumn({ label: " Account active sincet", onChange: setCreatedAt, selected: createdAt })}
              {renderColumn({ label: " Account status", onChange: setStatus, selected: status })}
              {renderColumn({ label: "  Remove", onChange: setRemove, selected: remove })}

            </DropdownMenu>
          </UncontrolledDropdown>
        </ButtonGroup>
      </div>

      <Table responsive className="tableStyle">
        <thead>
          <tr >
            {employeeName ? <th >Employee name</th> : null}
            {idCardNumber ? <th>Emirates ID</th> : null}
            {iban ? <th>IBAN</th> : null}
            {refId ? <th >Reference ID</th> : null}
            {position ? <th >Designation</th> : null}
            {department ? <th >Department</th> : null}
            {createdAt ? <th >Account active since</th> : null}
            {status ? <th >Account status</th> : null}
            {remove ? <th >Remove</th> : null}
          </tr>
        </thead>
        <tbody>

          {data?.employees?.map((item, i) => {
            return (
              <tr key={i}>
                {employeeName ? <td>{item.otherNames + " " + item.lastName || "-"}</td> : null}
                {idCardNumber ? <td>{item.idCardNumber}</td> : null}
                {iban ? <td>{item.wpsIban || "***"}</td> : null}
                {refId ? <td>{item.refId || "-"}</td> : null}
                {position ? <td>{item.position || "-"}</td> : null}
                {department ? <td>{item.department || "-"}</td> : null}
                {createdAt ? <td>{item.createdAt || "-"}</td> : null}
                {status ? <td>{item.status || "-"}</td> : null}
                {remove ? <td><Edit size={18} className="mr-2" /> <Trash2 size={18} color="#FF586B" /></td> : null}
              </tr>
            )
          })}

        </tbody>
      </Table>

    </>
  );
}
export default Employee
