import React, { Fragment, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Redirect, NavLink, useHistory } from "react-router-dom";
import { login } from "../../actions/authActions";
import logo from "../../img/logo.png";
import {
  Row,
  Col,
  Input,
  Form,
  FormGroup,
  Button,
  Label,
  Card,
  CardBody,
  CardFooter,
} from "reactstrap";
export const Login = (props) => {
  const [formData, setformData] = useState({
    username: "",
    password: "",
    errors: {
      username: "",
      password: "",
    },
  });

  const { username, password, errors } = formData;
  const history = useHistory();
  const dispatch = useDispatch();

  const isLoggedIn = useSelector((store) => store.authReducer.isLoggedIn);
  if (isLoggedIn) {
    return props.history.location.state ? (
      <Redirect to={props.history.location.state.from} />
    ) : (
      <Redirect to="/" />
    );
  }

  const handleChange = (e) => {
    setformData({ ...formData, [e.target.name]: e.target.value });
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    // if (validateForm()) return;
    dispatch(login(username, password, history));
  };
  return (
    <Fragment>
    <div className="container landing">
      <Row className="full-height-vh">
        <Col
          xs="12"
          className="d-flex align-items-center justify-content-center"
        >
          <Card className="gradient-dark-night text-center width-500">
            <CardBody className="wrapper layout-dark">
              <img src={logo} alt="" height="50" className="mt-4 mb-2" />
              <h4 className="white mb-4">Corporate portal</h4>
              <Form className="pt-2" onSubmit={handleSubmit}>
                <FormGroup>
                  <Col md="12" className="pull-left">
                    <Label for="inputEmail">Username</Label>
                    <Input
                      type="text"
                      className="form-control"
                      name="username"
                      id="inputEmail"
                      value={username}
                      onChange={handleChange}
                      placeholder="Username"
                      required
                    />
                  </Col>
                </FormGroup>

                <FormGroup>
                  <Col md="12" className="pull-left">
                    <Label for="inputPass">Password</Label>
                    <Input
                      type="password"
                      className="form-control"
                      name="password"
                      id="inputPass"
                      value={password}
                      onChange={handleChange}
                      placeholder="Password"
                      required
                    />
                  </Col>
                </FormGroup>

                <FormGroup>
                  <Row>
                    <Col md="12">
                      <div className="custom-control custom-checkbox mb-2 mr-sm-2 mb-sm-0 ml-3">
                        <Input
                          type="checkbox"
                          className="custom-control-input"
                          id="rememberme"
                        />
                        <Label
                          className="custom-control-label float-left white"
                          for="rememberme"
                        >
                          Remember Me
                        </Label>
                      </div>
                    </Col>
                  </Row>
                </FormGroup>
                <FormGroup>
                  <Col md="12">
                    <Button
                      type="submit"
                      color="primary"
                      block
                      className="btn-raised"
                    >
                      Submit
                    </Button>
                  </Col>
                </FormGroup>
              </Form>
            </CardBody>
            <CardFooter>
              <div className="float-left">
                <NavLink to="/forgot-password" className="text-white">
                  Forgot Password?
                </NavLink>
              </div>
              <div className="float-right">
                <NavLink to="/register" className="text-white">
                  Register Now
                </NavLink>
              </div>
            </CardFooter>
          </Card>
        </Col>
      </Row>
    </div>
    </Fragment>
  );
};

export default Login;
