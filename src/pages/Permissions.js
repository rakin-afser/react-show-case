import React, { useState, useEffect } from 'react';
import { useDispatch, useSelector } from "react-redux";
import {
  Row,
  Col,
  Input,
  Card,
  CardText,
  TabContent,
  TabPane,
  Nav,
  NavItem,
  NavLink,
  Button
} from 'reactstrap';
import classnames from 'classnames';
import { getRole, updatePermission } from "../../actions/authActions";

function Permissions(props) {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getRole())
  }, [dispatch])

  const UserRole = useSelector((state) => state.appReducer.userRole.roles)
  const [activeTab, setActiveTab] = useState('1');

  const toggle = tab => {
    if (activeTab !== tab) setActiveTab(tab);
  }

  //  function permissionUpdate() {
  //   dispatch(updatePermission())
  //  }

  return (
    <div>
      <Nav tabs>
        <NavItem>
          <NavLink
            className={classnames({ active: activeTab === '1' })}
            onClick={() => { toggle('1'); }}
          >
            <p className="text-bold-500 ">ADMIN</p>
          </NavLink>
        </NavItem>
        <NavItem>
          <NavLink
            className={classnames({ active: activeTab === '2' })}
            onClick={() => { toggle('2'); }}
          >

            <p className="text-bold-500 "> EXEC MANAGER</p>
          </NavLink>
        </NavItem>
        <NavItem>
          <NavLink
            className={classnames({ active: activeTab === '3' })}
            onClick={() => { toggle('3'); }}
          >

            <p className="text-bold-500 ">FINANCE</p>
          </NavLink>
        </NavItem>
        <NavItem>
          <NavLink
            className={classnames({ active: activeTab === '4' })}
            onClick={() => { toggle('4'); }}
          >
            <p className="text-bold-500 ">HR</p>
          </NavLink>
        </NavItem>
        <NavItem>
          <NavLink
            className={classnames({ active: activeTab === '5' })}
            onClick={() => { toggle('5'); }}
          >
            <p className="text-bold-500 "> CUSTOM 1</p>
          </NavLink>
        </NavItem>
        <NavItem>
          <NavLink
            className={classnames({ active: activeTab === '6' })}
            onClick={() => { toggle('6'); }}
          >

            <p className="text-bold-500 ">CUSTOM 2</p>
          </NavLink>
        </NavItem>
      </Nav>
      <TabContent activeTab={activeTab} responsive="true">
        {/* <Button style={{marginLeft:"90%"}}>Update</Button> */}
        <TabPane tabId="1">

          <Row>
            <Col sm="12">
              <Card body>
                {UserRole?.map((role) => {
                  return (
                    <Row style={{ marginLeft: "2%" }}>
                      <Col lg="4" >
                        <Input type="checkbox" name="" /><CardText>View company details</CardText>
                        <Input type="checkbox" name="exportEmployeeData" defaultChecked={role?.exportEmployeeData} /><CardText>Export employee data</CardText>
                        <Input type="checkbox" name="" /><CardText>Validate payments</CardText>
                        <Input type="checkbox" name="authorizeWPSBulkSalary" defaultChecked={role?.authorizeWPSBulkSalary} /><CardText>Authorize WPS salary</CardText>
                        <Input type="checkbox" name="business" defaultChecked={role?.business} /><CardText>Business access</CardText>
                      </Col>
                      <Col lg="4">
                        <Input type="checkbox" name="createUser" defaultChecked={role?.createUser} /><CardText>Create users</CardText>
                        <Input type="checkbox" name="uploadTransferProof" defaultChecked={role?.uploadTransferProof} /><CardText>Upload transfer proof</CardText>
                        <Input type="checkbox" /><CardText>Authorize payments</CardText>
                        <Input type="checkbox" name="validateWPSBulkSalary" defaultChecked={role?.validateWPSBulkSalary} /><CardText>Validate WPS salary</CardText>
                        <Input type="checkbox" name="addEmployee" defaultChecked={role?.addEmployee} /><CardText>Add employees</CardText>
                      </Col>
                      <Col lg="4">
                        <Input type="checkbox" /> <CardText>Create payments</CardText>
                        <Input type="checkbox" name="viewSalaryDetails" defaultChecked={role?.viewSalaryDetails} /><CardText>View payment details</CardText>
                        <Input type="checkbox" /><CardText>Create WPS payments</CardText>
                      </Col>
                    </Row>
                  )
                })[0]}

                {/* <Row style={{ marginLeft: "2%" }}>
                  <Col lg="4" >
                    <Input type="checkbox" name="" /><CardText>View company details</CardText>
                    <Input type="checkbox" name="exportEmployeeData" defaultChecked={UserRole?.[0].exportEmployeeData} /><CardText>Export employee data</CardText>
                    <Input type="checkbox" name="" /><CardText>Validate payments</CardText>
                    <Input type="checkbox" name="authorizeWPSBulkSalary" defaultChecked={UserRole?.[0].authorizeWPSBulkSalary} /><CardText>Authorize WPS salary</CardText>
                    <Input type="checkbox" name="business" defaultChecked={UserRole?.[0].business} /><CardText>Business access</CardText>
                  </Col>
                  <Col lg="4">
                    <Input type="checkbox" name="createUser" defaultChecked={UserRole?.[0].createUser} /><CardText>Create users</CardText>
                    <Input type="checkbox" name="uploadTransferProof" defaultChecked={UserRole?.[0].uploadTransferProof} /><CardText>Upload transfer proof</CardText>
                    <Input type="checkbox" /><CardText>Authorize payments</CardText>
                    <Input type="checkbox" name="validateWPSBulkSalary" defaultChecked={UserRole?.[0].validateWPSBulkSalary} /><CardText>Validate WPS salary</CardText>
                    <Input type="checkbox" name="addEmployee" defaultChecked={UserRole?.[0].addEmployee} /><CardText>Add employees</CardText>
                  </Col>
                  <Col lg="4">
                    <Input type="checkbox" /> <CardText>Create payments</CardText>
                    <Input type="checkbox" name="viewSalaryDetails" defaultChecked={UserRole?.[0].viewSalaryDetails} /><CardText>View payment details</CardText>
                    <Input type="checkbox" /><CardText>Create WPS payments</CardText>
                  </Col>
                </Row> */}

              </Card>
            </Col>
          </Row>
          <Button className="permission-btn">Update</Button>
        </TabPane>
        <TabPane tabId="2">

          <Row>
            <Col sm="12">
              <Card body>
              {UserRole?.map((role) => {
                  return (
                    <Row style={{ marginLeft: "2%" }}>
                      <Col lg="4" >
                        <Input type="checkbox" name="" /><CardText>View company details</CardText>
                        <Input type="checkbox" name="exportEmployeeData" defaultChecked={role?.exportEmployeeData} /><CardText>Export employee data</CardText>
                        <Input type="checkbox" name="" /><CardText>Validate payments</CardText>
                        <Input type="checkbox" name="authorizeWPSBulkSalary" defaultChecked={role?.authorizeWPSBulkSalary} /><CardText>Authorize WPS salary</CardText>
                        <Input type="checkbox" name="business" defaultChecked={role?.business} /><CardText>Business access</CardText>
                      </Col>
                      <Col lg="4">
                        <Input type="checkbox" name="createUser" defaultChecked={role?.createUser} /><CardText>Create users</CardText>
                        <Input type="checkbox" name="uploadTransferProof" defaultChecked={role?.uploadTransferProof} /><CardText>Upload transfer proof</CardText>
                        <Input type="checkbox" /><CardText>Authorize payments</CardText>
                        <Input type="checkbox" name="validateWPSBulkSalary" defaultChecked={role?.validateWPSBulkSalary} /><CardText>Validate WPS salary</CardText>
                        <Input type="checkbox" name="addEmployee" defaultChecked={role?.addEmployee} /><CardText>Add employees</CardText>
                      </Col>
                      <Col lg="4">
                        <Input type="checkbox" /> <CardText>Create payments</CardText>
                        <Input type="checkbox" name="viewSalaryDetails" defaultChecked={role?.viewSalaryDetails} /><CardText>View payment details</CardText>
                        <Input type="checkbox" /><CardText>Create WPS payments</CardText>
                      </Col>
                    </Row>
                  )
                })[1]}
              </Card>
            </Col>
          </Row>
          <Button className="permission-btn">Update</Button>
        </TabPane>
        <TabPane tabId="3">

          <Row >
            <Col sm="12">
              <Card body>
              {UserRole?.map((role) => {
                  return (
                    <Row style={{ marginLeft: "2%" }}>
                      <Col lg="4" >
                        <Input type="checkbox" name="" /><CardText>View company details</CardText>
                        <Input type="checkbox" name="exportEmployeeData" defaultChecked={role?.exportEmployeeData} /><CardText>Export employee data</CardText>
                        <Input type="checkbox" name="" /><CardText>Validate payments</CardText>
                        <Input type="checkbox" name="authorizeWPSBulkSalary" defaultChecked={role?.authorizeWPSBulkSalary} /><CardText>Authorize WPS salary</CardText>
                        <Input type="checkbox" name="business" defaultChecked={role?.business} /><CardText>Business access</CardText>
                      </Col>
                      <Col lg="4">
                        <Input type="checkbox" name="createUser" defaultChecked={role?.createUser} /><CardText>Create users</CardText>
                        <Input type="checkbox" name="uploadTransferProof" defaultChecked={role?.uploadTransferProof} /><CardText>Upload transfer proof</CardText>
                        <Input type="checkbox" /><CardText>Authorize payments</CardText>
                        <Input type="checkbox" name="validateWPSBulkSalary" defaultChecked={role?.validateWPSBulkSalary} /><CardText>Validate WPS salary</CardText>
                        <Input type="checkbox" name="addEmployee" defaultChecked={role?.addEmployee} /><CardText>Add employees</CardText>
                      </Col>
                      <Col lg="4">
                        <Input type="checkbox" /> <CardText>Create payments</CardText>
                        <Input type="checkbox" name="viewSalaryDetails" defaultChecked={role?.viewSalaryDetails} /><CardText>View payment details</CardText>
                        <Input type="checkbox" /><CardText>Create WPS payments</CardText>
                      </Col>
                    </Row>
                  )
                })[2]}
              </Card>
            </Col>
          </Row>
          <Button className="permission-btn">Update</Button>
        </TabPane>
        <TabPane tabId="4">

          <Row >
            <Col sm="12">
              <Card body>
              {UserRole?.map((role) => {
                  return (
                    <Row style={{ marginLeft: "2%" }}>
                      <Col lg="4" >
                        <Input type="checkbox" name="" /><CardText>View company details</CardText>
                        <Input type="checkbox" name="exportEmployeeData" defaultChecked={role?.exportEmployeeData} /><CardText>Export employee data</CardText>
                        <Input type="checkbox" name="" /><CardText>Validate payments</CardText>
                        <Input type="checkbox" name="authorizeWPSBulkSalary" defaultChecked={role?.authorizeWPSBulkSalary} /><CardText>Authorize WPS salary</CardText>
                        <Input type="checkbox" name="business" defaultChecked={role?.business} /><CardText>Business access</CardText>
                      </Col>
                      <Col lg="4">
                        <Input type="checkbox" name="createUser" defaultChecked={role?.createUser} /><CardText>Create users</CardText>
                        <Input type="checkbox" name="uploadTransferProof" defaultChecked={role?.uploadTransferProof} /><CardText>Upload transfer proof</CardText>
                        <Input type="checkbox" /><CardText>Authorize payments</CardText>
                        <Input type="checkbox" name="validateWPSBulkSalary" defaultChecked={role?.validateWPSBulkSalary} /><CardText>Validate WPS salary</CardText>
                        <Input type="checkbox" name="addEmployee" defaultChecked={role?.addEmployee} /><CardText>Add employees</CardText>
                      </Col>
                      <Col lg="4">
                        <Input type="checkbox" /> <CardText>Create payments</CardText>
                        <Input type="checkbox" name="viewSalaryDetails" defaultChecked={role?.viewSalaryDetails} /><CardText>View payment details</CardText>
                        <Input type="checkbox" /><CardText>Create WPS payments</CardText>
                      </Col>
                    </Row>
                  )
                })[3]}
              </Card>
            </Col>
          </Row>
          <Button className="permission-btn" >Update</Button>
        </TabPane>
        <TabPane tabId="5">

          <Row >
            <Col sm="12">
              <Card body>
              {UserRole?.map((role) => {
                  return (
                    <Row style={{ marginLeft: "2%" }}>
                      <Col lg="4" >
                        <Input type="checkbox" name="" /><CardText>View company details</CardText>
                        <Input type="checkbox" name="exportEmployeeData" defaultChecked={role?.exportEmployeeData} /><CardText>Export employee data</CardText>
                        <Input type="checkbox" name="" /><CardText>Validate payments</CardText>
                        <Input type="checkbox" name="authorizeWPSBulkSalary" defaultChecked={role?.authorizeWPSBulkSalary} /><CardText>Authorize WPS salary</CardText>
                        <Input type="checkbox" name="business" defaultChecked={role?.business} /><CardText>Business access</CardText>
                      </Col>
                      <Col lg="4">
                        <Input type="checkbox" name="createUser" defaultChecked={role?.createUser} /><CardText>Create users</CardText>
                        <Input type="checkbox" name="uploadTransferProof" defaultChecked={role?.uploadTransferProof} /><CardText>Upload transfer proof</CardText>
                        <Input type="checkbox" /><CardText>Authorize payments</CardText>
                        <Input type="checkbox" name="validateWPSBulkSalary" defaultChecked={role?.validateWPSBulkSalary} /><CardText>Validate WPS salary</CardText>
                        <Input type="checkbox" name="addEmployee" defaultChecked={role?.addEmployee} /><CardText>Add employees</CardText>
                      </Col>
                      <Col lg="4">
                        <Input type="checkbox" /> <CardText>Create payments</CardText>
                        <Input type="checkbox" name="viewSalaryDetails" defaultChecked={role?.viewSalaryDetails} /><CardText>View payment details</CardText>
                        <Input type="checkbox" /><CardText>Create WPS payments</CardText>
                      </Col>
                    </Row>
                  )
                })[4]}
              </Card>
            </Col>
          </Row>
          <Button className="permission-btn" >Update</Button>
        </TabPane>
        <TabPane tabId="6">

          <Row >
            <Col sm="12">
              <Card body>
              {UserRole?.map((role) => {
                  return (
                    <Row style={{ marginLeft: "2%" }}>
                      <Col lg="4" >
                        <Input type="checkbox" name="" /><CardText>View company details</CardText>
                        <Input type="checkbox" name="exportEmployeeData" defaultChecked={role?.exportEmployeeData} /><CardText>Export employee data</CardText>
                        <Input type="checkbox" name="" /><CardText>Validate payments</CardText>
                        <Input type="checkbox" name="authorizeWPSBulkSalary" defaultChecked={role?.authorizeWPSBulkSalary} /><CardText>Authorize WPS salary</CardText>
                        <Input type="checkbox" name="business" defaultChecked={role?.business} /><CardText>Business access</CardText>
                      </Col>
                      <Col lg="4">
                        <Input type="checkbox" name="createUser" defaultChecked={role?.createUser} /><CardText>Create users</CardText>
                        <Input type="checkbox" name="uploadTransferProof" defaultChecked={role?.uploadTransferProof} /><CardText>Upload transfer proof</CardText>
                        <Input type="checkbox" /><CardText>Authorize payments</CardText>
                        <Input type="checkbox" name="validateWPSBulkSalary" defaultChecked={role?.validateWPSBulkSalary} /><CardText>Validate WPS salary</CardText>
                        <Input type="checkbox" name="addEmployee" defaultChecked={role?.addEmployee} /><CardText>Add employees</CardText>
                      </Col>
                      <Col lg="4">
                        <Input type="checkbox" /> <CardText>Create payments</CardText>
                        <Input type="checkbox" name="viewSalaryDetails" defaultChecked={role?.viewSalaryDetails} /><CardText>View payment details</CardText>
                        <Input type="checkbox" /><CardText>Create WPS payments</CardText>
                      </Col>
                    </Row>
                  )
                })[5]}
              </Card>
            </Col>
          </Row>
          <Button className="permission-btn">Update</Button>
        </TabPane>
      </TabContent>

    </div>
  );
}

export default Permissions;