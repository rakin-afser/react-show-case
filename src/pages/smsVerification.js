import React, { useState, useEffect } from "react";
import { useDispatch } from "react-redux";
import { NavLink, useHistory } from "react-router-dom";
import { parse } from "qs";
import { smsVerification } from "../../actions/authActions";
import logo from "../../assets/img/logo.png";
import {
  Row,
  Col,
  Form,
  FormGroup,
  Button,
  Card,
  CardBody,
} from "reactstrap";
function SmsVerification(props) {
  const [id, setId] = useState(null);
  const history = useHistory();
  useEffect(() => {
    const { id } = parse(props.location.search, { ignoreQueryPrefix: true });
    setId(id);
  }, []);
  const [smsCodeData, setsmsCodeData] = useState({
    smsCode: "",
    errors: {
      smsCode: "",
    },
  });

  const { smsCode, errors } = smsCodeData;

  const dispatch = useDispatch();

  const handleChange = (e) => {
    setsmsCodeData({ ...smsCodeData, [e.target.name]: e.target.value });
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    dispatch(smsVerification(id, smsCode, history));
  };
  return (
    <div className="container landing">
      <Row>
        <Col xs="5"> </Col>
        <Col xs="3">
          <img src={logo} alt="" height="50" className="mt-4 mb-2" />
        </Col>
        <Col xs="4"></Col>
      </Row>
      <Row>
        <Col xs="5"> </Col>
        <Col xs="3">
          <h6 className="text-white">Corporate portal</h6>
        </Col>
        <Col xs="4"></Col>
      </Row>
      <Row>
        <Col
          xs="12"
          className="d-flex align-items-center justify-content-center"
        >
          <Card className="gradient-dark-night text-center width-500">
            <CardBody className="wrapper layout-dark">
              <h4 className="white mb-4">SMS verification</h4>
              <Form className="pt-2" onSubmit={handleSubmit}>
                <FormGroup>
                  <Col md="12">
                    <p className="text-muted mb-0 font-13">
                      When you log in first time or from a different device or
                      network we send you an SMS code to your registered number
                      to verify it's really you for security reasons.
                      <br />
                      We have sent you a 4 digit code, please enter it below to
                      continue.
                    </p>
                  </Col>
                </FormGroup>

                <FormGroup>
                  <Col md="12">
                    <label htmlFor="emailaddress">SMS code</label>
                    <input
                      className="form-control"
                      type="password"
                      maxLength="6"
                      name="smsCode"
                      onChange={handleChange}
                      autoFocus
                      required
                    />
                  </Col>
                  <br />
                  <FormGroup>
                    <Col md="12">
                      <Button
                        type="submit"
                        color="primary"
                        block
                        className="btn-raised"
                      >
                        Submit
                      </Button>
                    </Col>
                  </FormGroup>
                </FormGroup>
              </Form>
            </CardBody>
          </Card>
        </Col>
      </Row>
      <Row>
        <Col xs="5"> </Col>
        <Col xs="3">
          <div className="text-white">
            <p>
              Back To <span></span>
              <NavLink to="/login" className="text-white">
                Log in
              </NavLink>
            </p>
          </div>
        </Col>
        <Col xs="4"></Col>
      </Row>
    </div>
  );
}

export default SmsVerification;
