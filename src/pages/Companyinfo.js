import React, { useState, useEffect } from 'react';
import {
  Row,
  Col,
  TabContent,
  TabPane,
  Nav,
  NavItem,
  NavLink,
  Container,
} from 'reactstrap';
import classnames from "classnames";
import { Edit } from "react-feather";
import { useDispatch, useSelector } from 'react-redux';
import { getCompanyProfile } from '../../actions/companyProfileAction';

const textStyle = {
  color: "gray"
}

function Companyinfo(props) {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getCompanyProfile())
  }, [])

  const { bankDetails, companyDetails, ubo, managementDetails, legalName, tradingName } = useSelector((state) => state.appReducer.companyProfile)
  const [activeTab, setActiveTab] = useState("1")

  const toggle = (tab) => {
    if (activeTab !== tab)
      setActiveTab(tab)
  }
  return (


    <div>
      <div>
        <Nav tabs>
          <NavItem>
            <NavLink
              className={classnames({
                active: activeTab === "1"
              })}
              onClick={() => {
                toggle("1");
              }}
            >
              <h6> Company Details</h6>
            </NavLink>
          </NavItem>
          <NavItem>
            <NavLink
              className={classnames({
                active: activeTab === "2"
              })}
              onClick={() => {
                toggle("2");
              }}
            >
              <h6> Ownership Details</h6>
            </NavLink>
          </NavItem>
          <NavItem>
            <NavLink
              className={classnames({
                active: activeTab === "3"
              })}
              onClick={() => {
                toggle("3");
              }}
            >
              <h6> Funding Bank Account Details</h6>
            </NavLink>
          </NavItem>
          <NavItem>
            <NavLink
              className={classnames({
                active: activeTab === "4"
              })}
              onClick={() => {
                toggle("4");
              }}
            >
              <h6>Management Details</h6>
            </NavLink>
          </NavItem>
        </Nav>
      </div>
      <Container>
        <TabContent activeTab={activeTab}>
          <TabPane tabId="1">
            <Row>
              <Col sm="12">
                <div >
                  <h5 className="font-medium-2" style={textStyle} >Company details <span className="small-heading-font"><a href="/updateCompanyDetails" target="_blank"><Edit size="2%" /></a></span></h5>
                  <div className="row mb-2">
                    <div className="col-sm-4">
                      <h6 className="font-small-4" style={textStyle}>Company legal name</h6>
                    </div>
                    <div className="col-sm-4" >
                      {legalName && legalName}
                    </div>
                  </div>
                  <div className="row mb-2">
                    <div className="col-sm-4">
                      <h6 className="font-small-4" style={textStyle}>Company trading name</h6>
                    </div>
                    <div className="col-sm-4">
                      {tradingName}
                    </div>
                  </div>
                  <div className="row mb-2">
                    <div className="col-sm-4">
                      <h6 className="font-small-4" style={textStyle}>Company website link</h6>
                    </div>
                    <div className="col-sm-4">
                      {companyDetails?.website}
                    </div>
                  </div>
                  <div className="row mb-2">
                    <div className="col-sm-4">
                      <h6 className="font-small-4" style={textStyle}>Company trade license</h6>
                    </div>
                    <div className="col-sm-4">
                      <a
                        href="#"
                      >
                        {companyDetails?.tradeLicense?.image}
                      </a>

                    </div>
                  </div>
                  <div className="row mb-2">
                    <div className="col-sm-4">
                      <h6 className="font-small-4" style={textStyle}>Company trade license number</h6>
                    </div>
                    <div className="col-sm-4">
                      {companyDetails?.tradeLicense?.licenseNumber}
                    </div>
                  </div>
                  <div className="row mb-2">
                    <div className="col-sm-4">
                      <h6 className="font-small-4" style={textStyle}>
                        Company trade license issuing authority
                 </h6>
                    </div>
                    <div className="col-sm-4">

                      {companyDetails?.tradeLicense?.issuingAuthority}
                    </div>
                  </div>
                  <div className="row mb-2">
                    <div className="col-sm-4">
                      <h6 className="font-small-4" style={textStyle}>Company establishment date</h6>
                    </div>
                    <div className="col-sm-4">

                      {companyDetails?.tradeLicense?.establishmentDate.substring(10, 0)}
                    </div>
                  </div>
                  <div className="row mb-2">
                    <div className="col-sm-4">
                      <h6 className="font-small-4" style={textStyle}>Company registered address</h6>
                    </div>
                    <div className="col-sm-4">
                      {companyDetails?.tradeLicense?.registeredAddress?.addressLine1}<br />
                      {companyDetails?.tradeLicense?.registeredAddress?.addressLine2}<br />
                      {companyDetails?.tradeLicense?.registeredAddress?.addressLine3}<br />
                      {companyDetails?.tradeLicense?.registeredAddress?.city}<br />
                      {companyDetails?.tradeLicense?.registeredAddress?.poBox}<br />
                      {companyDetails?.tradeLicense?.registeredAddress?.country}

                    </div>
                  </div>
                  <div className="row mb-2">
                    <div className="col-sm-4">
                      <h6 className="font-small-4" style={textStyle}>Company correspondance address</h6>
                    </div>

                    <div className="col-sm-4">
                      {companyDetails?.tradeLicense?.registeredAddress?.addressLine1}<br />
                      {companyDetails?.tradeLicense?.registeredAddress?.addressLine2}<br />
                      {companyDetails?.tradeLicense?.registeredAddress?.addressLine3}<br />
                      {companyDetails?.tradeLicense?.registeredAddress?.city}<br />
                      {companyDetails?.tradeLicense?.registeredAddress?.poBox}<br />
                      {companyDetails?.tradeLicense?.registeredAddress?.country}
                    </div>
                  </div>
                  <div className="row mb-2">
                    <div className="col-sm-4">
                      <h6 className="font-small-4" style={textStyle}>Company MOA</h6>
                    </div>
                    <div className="col-sm-4">
                      <a
                        href="#"
                      >
                        {companyDetails?.moa}

                      </a
                      >
                    </div>
                  </div>
                  <div className="row mb-2">
                    <div className="col-sm-4">
                      <h6 className="font-small-4" style={textStyle}>Type of entity</h6>
                    </div>
                    <div className="col-sm-4">
                      {companyDetails?.typeOfEntity}

                    </div>
                  </div>
                  <div className="row mb-2">
                    <div className="col-sm-4">
                      <h6 className="font-small-4" style={textStyle}>Country of registration</h6>
                    </div>
                    <div className="col-sm-4">
                      {companyDetails?.countryOfRegistration}

                    </div>
                  </div>
                  <div className="row mb-2">
                    <div className="col-sm-4">
                      <h6 className="font-small-4" style={textStyle}>Nature of business</h6>
                    </div>
                    <div className="col-sm-4">

                      {companyDetails?.natureOfBusiness}
                    </div>
                  </div>
                </div>

              </Col>
            </Row>
          </TabPane>
          <TabPane tabId="2">
            <Row>
              <Col sm="12">

                <div>
                  <h5 className="font-medium-2" style={textStyle}>Company details <span className="small-heading-font"><a href="/updateCompanyDetails" target="_blank"><Edit size="2%" /></a></span></h5>
                  <h6 style={textStyle}>Ultimate business owner </h6>
                  <div className="row mb-2">
                    <div className="col-sm-4">
                      <h6 className="font-small-4" style={textStyle}>Name</h6>
                    </div>
                    <div className="col-sm-4">
                      {ubo?.length > 0 && ubo[0].name}
                    </div>
                  </div>
                  <div className="row mb-2">
                    <div className="col-sm-4">
                      <h6 className="font-small-4" style={textStyle}>Title</h6>
                    </div>
                    <div className="col-sm-4">

                      {ubo?.length > 0 && ubo[0].title || "N/A"}
                    </div>
                  </div>
                  <div className="row mb-2">
                    <div className="col-sm-4">
                      <h6 className="font-small-4" style={textStyle}>% Shareholding</h6>
                    </div>
                    <div className="col-sm-4">
                      {ubo?.length > 0 && ubo[0].share}
                    </div>
                  </div>
                  <div className="row mb-2">
                    <div className="col-sm-4">
                      <h6 className="font-small-4" style={textStyle}>Date of Birth</h6>
                    </div>
                    <div className="col-sm-4">
                      {ubo?.length > 0 && ubo[0].dateofBirth || "N/A"}
                    </div>
                  </div>
                  <div className="row mb-2">
                    <div className="col-sm-4">
                      <h6 className="font-small-4" style={textStyle}>Passport Number</h6>
                    </div>
                    <div className="col-sm-4">

                      {ubo?.length > 0 && ubo[0].passport?.passportNumber}
                    </div>
                  </div>
                  <div className="row mb-2">
                    <div className="col-sm-4">
                      <h6 className="font-small-4" style={textStyle}>
                        Nationality
                 </h6>
                    </div>
                    <div className="col-sm-4">

                      {ubo?.length > 0 && ubo[0].countryOfResidence}
                    </div>
                  </div>
                  <div className="row mb-2">
                    <div className="col-sm-4">
                      <h6 className="font-small-4" style={textStyle}>Passport issue date</h6>
                    </div>
                    <div className="col-sm-4">
                      {ubo?.length > 0 && ubo[0].passport?.issue.substring(10, 0)}
                    </div>
                  </div>
                  <div className="row mb-2">
                    <div className="col-sm-4">
                      <h6 className="font-small-4" style={textStyle}>Passport expiry date</h6>
                    </div>
                    <div className="col-sm-4">
                      {ubo?.length > 0 && ubo[0].passport?.expiry.substring(10, 0)}
                    </div>
                  </div>
                  <div className="row mb-2">
                    <div className="col-sm-4">
                      <h6 className="font-small-4" style={textStyle}>Country of residence</h6>
                    </div>

                    <div className="col-sm-4">
                      {ubo?.length > 0 && ubo[0].countryOfResidence}
                    </div>
                  </div>
                  <div className="row mb-2">
                    <div className="col-sm-4">
                      <h6 className="font-small-4" style={textStyle}>Profession</h6>
                    </div>
                    <div className="col-sm-4">
                      {ubo?.length > 0 && ubo[0].profession}

                    </div>
                  </div>
                  <div className="row mb-2">
                    <div className="col-sm-4">
                      <h6 className="font-small-4" style={textStyle}>Passport Copy</h6>
                    </div>
                    <div className="col-sm-4">
                      <a
                        href="#"
                      >Passport copy pdf</a
                      >
                    </div>
                  </div>
                </div>

              </Col>
            </Row>
          </TabPane>
          <TabPane tabId="3">
            <Row>
              <Col sm="12">
                <div>
                  <h5 className="font-medium-2" style={textStyle}>Funding bank account details <span className="small-heading-font"><a href="/funding-bank-details" target="_blank"><Edit size="2%" /></a></span></h5>
                  <div className="row mb-2">
                    <div className="col-sm-4">
                      <h6 className="font-small-4" style={textStyle}>Bank name</h6>
                    </div>
                    <div className="col-sm-4">

                      {bankDetails?.bankName}
                    </div>
                  </div>
                  <div className="row mb-2">
                    <div className="col-sm-4">
                      <h6 className="font-small-4" style={textStyle}>Bank account number</h6>
                    </div>
                    <div className="col-sm-4">
                      {bankDetails?.accountNumber}

                    </div>
                  </div>
                  <div className="row mb-2">
                    <div className="col-sm-4">
                      <h6 className="font-small-4" style={textStyle} >Bank branch address</h6>
                    </div>
                    <div className="col-sm-4">
                      {bankDetails?.address}
                    </div>
                  </div>
                  <div className="row mb-2">
                    <div className="col-sm-4">
                      <h6 className="font-small-4" style={textStyle}>IBAN</h6>
                    </div>
                    <div className="col-sm-4">
                      {bankDetails?.iban}
                    </div>
                  </div>
                  <div className="row mb-2">
                    <div className="col-sm-4">
                      <h6 className="font-small-4" style={textStyle}>Account type</h6>
                    </div>
                    <div className="col-sm-4">
                      {bankDetails?.accountType}

                    </div>
                  </div>
                  <div className="row mb-2">
                    <div className="col-sm-4">
                      <h6 className="font-small-4" style={textStyle}>
                        Account Since
                 </h6>
                    </div>
                    <div className="col-sm-4">
                      {bankDetails?.accountSince || "N/A"}
                    </div>
                  </div>
                  <div className="row mb-2">
                    <div className="col-sm-4">
                      <h6 className="font-small-4" style={textStyle}>Bank Letter</h6>
                    </div>
                    <div className="col-sm-4">
                      <a href="#">
                        {bankDetails?.bankLetter}

                      </a>
                    </div>
                  </div>
                </div>
              </Col>
            </Row>
          </TabPane>
          <TabPane tabId="4">
            <Row>
              <Col sm="12">
                <div>
                  <h5 className="font-medium-2" style={textStyle}>Management Details <span ><a href="/updateCompanyDetails" target="_blank"><Edit size="2%" /></a></span></h5>
                  <h6 className="font-small-4" style={textStyle}>Account manager </h6>

                  <div className="row mb-2">
                    <div className="col-sm-4">
                      <h6 className="font-small-4" style={textStyle}>Name</h6>
                    </div>
                    <div className="col-sm-4">

                      {managementDetails?.accountManagers.length > 0 && managementDetails?.accountManagers[0].name}
                    </div>
                  </div>
                  <div className="row mb-2">
                    <div className="col-sm-4">
                      <h6 className="font-small-4" style={textStyle}>Title</h6>
                    </div>
                    <div className="col-sm-4">

                      {managementDetails?.accountManagers.length > 0 && managementDetails?.accountManagers[0].title || "N/A"}
                    </div>
                  </div>
                  <div className="row mb-2">
                    <div className="col-sm-4">
                      <h6 className="font-small-4" style={textStyle}>% Shareholding</h6>
                    </div>
                    <div className="col-sm-4">
                      {managementDetails?.accountManagers.length > 0 && managementDetails?.accountManagers[0].share}
                    </div>
                  </div>
                  <div className="row mb-2">
                    <div className="col-sm-4">
                      <h6 className="font-small-4" style={textStyle}>Date of Birth</h6>
                    </div>
                    <div className="col-sm-4">
                      {managementDetails?.accountManagers.length > 0 && managementDetails?.accountManagers[0].dateofBirth || "N/A"}
                    </div>
                  </div>
                  <div className="row mb-2">
                    <div className="col-sm-4">
                      <h6 className="font-small-4" style={textStyle}>Passport Number</h6>
                    </div>
                    <div className="col-sm-4">

                      {managementDetails?.accountManagers.length > 0 && ubo[0].passport?.passportNumber}
                    </div>
                  </div>
                  <div className="row mb-2">
                    <div className="col-sm-4">
                      <h6 className="font-small-4" style={textStyle}>
                        Nationality
                 </h6>
                    </div>
                    <div className="col-sm-4">

                      {managementDetails?.accountManagers.length > 0 && managementDetails?.accountManagers[0].countryOfResidence}
                    </div>
                  </div>
                  <div className="row mb-2">
                    <div className="col-sm-4">
                      <h6 className="font-small-4" style={textStyle}>Passport issue date</h6>
                    </div>
                    <div className="col-sm-4">
                      {managementDetails?.accountManagers.length > 0 && managementDetails?.accountManagers[0].passport?.issue.substring(10, 0)}
                    </div>
                  </div>
                  <div className="row mb-2">
                    <div className="col-sm-4">
                      <h6 className="font-small-4" style={textStyle}>Passport expiry date</h6>
                    </div>
                    <div className="col-sm-4">
                      {managementDetails?.accountManagers.length > 0 && managementDetails?.accountManagers[0].passport?.expiry.substring(10, 0)}
                    </div>
                  </div>
                  <div className="row mb-2">
                    <div className="col-sm-4">
                      <h6 className="font-small-4" style={textStyle}> Country of residence</h6>
                    </div>

                    <div className="col-sm-4">
                      {managementDetails?.accountManagers.length > 0 && managementDetails?.accountManagers[0].countryOfResidence}
                    </div>
                  </div>
                  <div className="row mb-2">
                    <div className="col-sm-4">
                      <h6 className="font-small-4" style={textStyle}>Profession</h6>
                    </div>
                    <div className="col-sm-4">
                      {managementDetails?.accountManagers.length > 0 && managementDetails?.accountManagers[0].profession}

                    </div>
                  </div>
                  <div className="row mb-2">
                    <div className="col-sm-4">
                      <h6 className="font-small-4" style={textStyle}>Passport Copy</h6>
                    </div>
                    <div className="col-sm-4">
                      <a
                        href="#"
                      >Passport copy pdf</a
                      >
                    </div>
                  </div>
                </div>
              </Col>
            </Row>
          </TabPane>
        </TabContent>
      </Container>
    </div>
  );
}

export default Companyinfo;
