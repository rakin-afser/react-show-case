import React, { useEffect, useState } from 'react';
import {
    UncontrolledTooltip,
    Row,
    Col,
    Card,
    CardBody,
} from "reactstrap";
import { Edit, Slash } from "react-feather";
import { useSelector, useDispatch } from "react-redux";
import { getUserList } from "../../actions/authActions";
import avatarm8 from "../../assets/img/portrait/small/avatar-s-5.png"
import AddUser from "../modals/AddUser"
import { updateRoleStatus } from "../../actions/authActions";

const iconEdit = {
    marginLeft: "80%",
}

function Users(props) {

    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(getUserList())
    }, [dispatch])

    const [editable, setEditable] = useState(true)

    const [isshowAddUsermodal, setAddUserModal] = useState(false)
    const [fullName, setFullName] = useState('')
    const [userName, setUserName] = useState('')
    const [email, setEmail] = useState('')
    const [idCardNumber, setIdCardNumber] = useState('')
    const [mobileNumber, setMobileNumber] = useState('')
    const [userId, setUserId] = useState('')
    const [roleId, setRoleId] = useState('')
    const [active, setActive] = useState(false)
    const userLists = useSelector(state => state.appReducer.userList.companyUsers)


    function deActivate(userlist) {
        if (userlist && userlist._id) {
            setUserId(userlist._id)
        }

        dispatch((updateRoleStatus(userId, active)))

    }

    function editModal(userlist) {

        if (userlist && userlist._id) {
            setUserId(userlist._id)
            setFullName(userlist.fullName)
            setUserName(userlist.username)
            setEmail(userlist.email)
            setIdCardNumber(userlist.idCardNumber)
            setMobileNumber(userlist.mobileNumber)
            setRoleId(userlist?.role?._id)
        }

        setAddUserModal(!isshowAddUsermodal)
    }


    return (

        <div >

            <Row className="d-flex mb-3 py-2">

                {
                    userLists?.map((userlist) => {
                        return (
                            <Col sm="4" md="4" key={userlist._id} className="d-flex align-items-center justify-content-center">
                                <Card style={{ width: "84%" }} >
                                    <CardBody>
                                        <div style={iconEdit}>
                                            <a onClick={() => { editModal(userlist) }} > <Edit size={15} /></a>
                                            <a id="UncontrolledTooltipExample" onClick={() => deActivate(userlist)}>

                                                <Slash style={{ marginLeft: "5px" }} size={15} />
                                            </a>
                                            <UncontrolledTooltip placement="right" target="UncontrolledTooltipExample">
                                                Deactivate
                                            </UncontrolledTooltip>
                                        </div>
                                        <Row >
                                            <Col sm="12" className="text-center">
                                                <img
                                                    src={avatarm8}
                                                    className="rounded-circle img-fluid mb-4" alt="Card cap 02"
                                                />
                                                <h6 className=" text-center info" >
                                                    {userlist?.fullName}
                                                </h6>
                                                <span className="font-small-4   ">
                                                    User Name: {userlist?.username}
                                                </span><br />
                                                <span className="font-small-4  ">
                                                    Created at: {userlist?.createdAt.substring(10, 0)}
                                                </span><br />
                                                <span className="font-small-3   ">
                                                    Role: {userlist?.role?.name}
                                                </span>
                                            </Col>
                                        </Row>
                                    </CardBody>
                                </Card>
                            </Col>
                        )
                    })
                }

            </Row>


            <AddUser isOpen={isshowAddUsermodal} toggle={setAddUserModal}
                fullName={fullName} userName={userName} email={email} idCardNumber={idCardNumber} mobileNumber={mobileNumber}
                userId={userId} roleId={roleId} editable={editable}
            />
        </div>
    );
}

export default Users;